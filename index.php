<?php
    require_once('config/config.php');
    require_once(APP_ROOT . '/module/Common/SessionService.php');
?>

<?php
    unsetUserSession();
?>

<?php
    require_once(APP_ROOT . '/module/Common/view/header.php')
?>

<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header"
    style="background: url('<?=SITE_ROOT?>/img/bg2.gif') center/cover no-repeat;">
    <div class="app-main">
        <?php
            require_once(APP_ROOT . '/module/Auth/login-register.form.php');
        ?>
    </div>
</div> <!--app-container-->

<?php
    require_once('module/Common/view/scripts.php')
?>

<?php
    require_once('module/Common/view/footer.php')
?>