CREATE DATABASE  IF NOT EXISTS `e-bulletin` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `e-bulletin`;
-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: e-bulletin
-- ------------------------------------------------------
-- Server version	5.7.15-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `announcements`
--

DROP TABLE IF EXISTS `announcements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `announcements` (
  `announcement_id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `posted_by_user_id` int(11) NOT NULL,
  PRIMARY KEY (`announcement_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `announcements`
--

LOCK TABLES `announcements` WRITE;
/*!40000 ALTER TABLE `announcements` DISABLE KEYS */;
INSERT INTO `announcements` VALUES (1,'Announcement 1c','On December 25, 2019, all people around the world will celebrate Christmas. We are inviting you to join our miny celebration at the New Gymnasium. Wear any decent attire that you like. On December 25, 2019, all people around the world will celebrate Christmas. We are inviting you to join our miny celebration at the New Gymnasium. Wear any decent attire that you like. On December 25, 2019, all people around the world will celebrate Christmas. ','2004-01-01','00:00:00',1),(7,'Test 1','Test 1','2019-10-28','20:00:02',1);
/*!40000 ALTER TABLE `announcements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `colleges`
--

DROP TABLE IF EXISTS `colleges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `colleges` (
  `college_id` int(11) NOT NULL AUTO_INCREMENT,
  `college_name` varchar(255) NOT NULL,
  PRIMARY KEY (`college_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `colleges`
--

LOCK TABLES `colleges` WRITE;
/*!40000 ALTER TABLE `colleges` DISABLE KEYS */;
INSERT INTO `colleges` VALUES (1,'Not in a college'),(2,'College of Information Technology'),(3,'College of Arts and Sciences'),(4,'Test College 0'),(5,'Test College 1');
/*!40000 ALTER TABLE `colleges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `posted_by_user_id` int(11) NOT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` VALUES (1,'Outreach Program 2019','On December 25, 2019, all people around the world will celebrate Christmas. We are inviting you to join our miny celebration at the New Gymnasium. Wear any decent attire that you like. On December 25, 2019, all people around the world will celebrate Christmas. We are inviting you to join our miny celebration at the New Gymnasium. Wear any decent attire that you like. On December 25, 2019, all people around the world will celebrate Christmas. ','/img/events/Outreach Program 2019_1572247340.jpg','2019-10-27','21:41:00',1),(3,'Test 1','Test 1 Body','/img/events/Test 1_1572263986.jpg','2019-10-28','08:42:11',1);
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `organizations`
--

DROP TABLE IF EXISTS `organizations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organizations` (
  `organization_id` int(11) NOT NULL AUTO_INCREMENT,
  `organization_name` varchar(255) NOT NULL,
  PRIMARY KEY (`organization_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organizations`
--

LOCK TABLES `organizations` WRITE;
/*!40000 ALTER TABLE `organizations` DISABLE KEYS */;
INSERT INTO `organizations` VALUES (1,'Org 2');
/*!40000 ALTER TABLE `organizations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `organizations_members`
--

DROP TABLE IF EXISTS `organizations_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organizations_members` (
  `user_id` int(11) NOT NULL,
  `organization_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`organization_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organizations_members`
--

LOCK TABLES `organizations_members` WRITE;
/*!40000 ALTER TABLE `organizations_members` DISABLE KEYS */;
/*!40000 ALTER TABLE `organizations_members` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_credentials`
--

DROP TABLE IF EXISTS `users_credentials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_credentials` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_number` varchar(10) NOT NULL,
  `password` varchar(255) NOT NULL,
  `user_type_id` int(2) NOT NULL,
  `user_info_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `idnumber_UNIQUE` (`id_number`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_credentials`
--

LOCK TABLES `users_credentials` WRITE;
/*!40000 ALTER TABLE `users_credentials` DISABLE KEYS */;
INSERT INTO `users_credentials` VALUES (1,'1000','1000',1,1),(2,'2000','2000',2,2),(3,'2001','2001',2,3),(4,'3000','3000',3,4),(5,'3001','3001',3,5),(6,'3002','3002',3,6),(7,'4000','4000',4,7),(8,'4001','4001',4,8),(10,'4003','4003',4,10),(11,'12-10020','12-10020',1,11),(12,'123123','123123',1,12),(14,'4353534','345345345',2,14);
/*!40000 ALTER TABLE `users_credentials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_info`
--

DROP TABLE IF EXISTS `users_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_info` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `middle_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `phone_number` varchar(13) NOT NULL,
  `college_id` int(2) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_info`
--

LOCK TABLES `users_info` WRITE;
/*!40000 ALTER TABLE `users_info` DISABLE KEYS */;
INSERT INTO `users_info` VALUES (1,'Marjorie','S','Magbitang','09123456789',1),(2,'Staff 1','Staff 1','Staff 1','09345678912',1),(3,'Staff 2','S','Staff 2','09456789123',1),(4,'Jay-Ar','G','Balauag','09234567891',2),(5,'King Christian','D','Antonio','09121212121',2),(6,'Jayson','S','Nacorda','09232323232',2),(7,'Student 1','Student 1','Student 1','09989898898',2),(8,'Student 2','Student 2','Student 2','09747372713',2),(10,'Student 4','Student 4','Student 4','09234235632',3),(11,'Admin F','Admin M','Admin S','09167691779',1),(12,'Admin F2','Admin M 2','Admin S2','09178787787',1),(13,'Staff 3','Staff 3','Staff 3','09234234141',3),(14,'Staff 3','Staff 3','Staff 3','09234234141',3),(15,'1000','1000','1000','1000',1),(16,'1000','1000','1000','1000',1),(17,'1000','1000','1000','1000',1),(18,'1000','1000','1000','1000',1);
/*!40000 ALTER TABLE `users_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_type`
--

DROP TABLE IF EXISTS `users_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_type` (
  `user_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `access_label` varchar(45) NOT NULL,
  `access_level` int(1) NOT NULL,
  `icon` varchar(45) NOT NULL,
  PRIMARY KEY (`user_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_type`
--

LOCK TABLES `users_type` WRITE;
/*!40000 ALTER TABLE `users_type` DISABLE KEYS */;
INSERT INTO `users_type` VALUES (1,'Admin',2,'fa fa-user-cog'),(2,'Staff',1,'fa fa-user-alt'),(3,'Faculty',1,'fa fa-chalkboard-teacher'),(4,'Student',1,'fa fa-user-graduate');
/*!40000 ALTER TABLE `users_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'e-bulletin'
--

--
-- Dumping routines for database 'e-bulletin'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-30 22:32:08
