<?php
    require_once('../../config/config.php');
    require_once('../../config/module-paths.php');
    require_once(APP_ROOT . '/module/Common/SessionService.php');
    require_once(APP_ROOT . '/module/Common/Database.php');
    require_once(APP_ROOT . '/module/Common/Logger.php');
    require_once(APP_ROOT . '/module/Common/Utilities.php');
?>

<?php
    if (!isLoggedIn()) {
        header('Location: ' . LOGIN_REGISTER_FORM);
        exit;
    }
?>

<?php
    require_once(APP_ROOT . '/module/Common/view/header.php')
?>

<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
    <?php
        require_once(APP_ROOT . '/module/Common/view/top-bar.php');
    ?>
    <div class="app-main">
        <?php
            $leftNavData = array('active_link' => 'Colleges');
            require_once(APP_ROOT . '/module/Common/view/left-nav.php');
        ?>

        <div class="app-main__outer">
            <div class="app-main__inner">
                <?php
                    $pageContentHeaderData = array(
                        'icon' => 'pe-7s-comment',
                        'title' => 'Colleges',
                        'description' => 'Manage colleges.',
                        'features' => 'Add/Edit Colleges',
                        'action_dropdown_options' => array(
                            array('href' => COLLEGE_ADD, 'label' => 'Add College', 'icon' => 'fa fa-plus fa-xs')
                        ),
                        'back_button_href' => COLLEGE_LIST,
                        'options' => array(
                            'show_action_dropdown' => true,
                            'show_back_button' => false
                        )
                    );
                    require_once(APP_ROOT . '/module/Common/view/page-content-header.php')
                ?>
                <div class="row">
                    <div class="col-md-12 overflow-auto">
                        <table id="announcements_table" class="display table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th style="min-width: 100px;">College Name</th>
                                    <th style="max-width: 100px;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                try {
                                    $getColleges = 'SELECT college_id, college_name FROM colleges';
                                    $stmt = $conn->prepare($getColleges);
                                    $stmt->execute();

                                    if ($stmt->rowCount() > 0) {
                                        $colleges = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                        foreach ($colleges as $college) {
                                            echo '
                                            <tr>
                                                    <td>' . $college['college_id'] . '</td>
                                                    <td>' . $college['college_name'] . '</td>
                                                    <td>
                                                    <form class="d-inline-block mt-2 mb-2" action="'. COLLEGE_EDIT .'" method="get">
                                                        <button class="btn btn-success btn-sm" style="width: 100px;">Edit</button>
                                                        <input type="hidden" name="id" value="' . $college['college_id'] . '">
                                                    </form>
                                                    </td>
                                            </tr>
                                            ';
                                        }
                                    }
                                } catch (PDOException $e) {
                                    logError($e->getMessage());
                                }
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th style="min-width: 100px;">College Name</th>
                                    <th style="max-width: 100px;">Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!--app-container-->

<?php
    require_once(APP_ROOT . '/module/Common/view/scripts.php')
?>

<script>
    $(document).ready(function() {
        $('#announcements_table').DataTable( {
            "paging":   true,
            "ordering": true,
            "info":     false
        });
    });
</script>

<?php
    require_once(APP_ROOT . '/module/Common/view/footer.php')
?>