<?php
    require_once('../../config/config.php');
    require_once('../../config/module-paths.php');
    require_once(APP_ROOT . '/module/Common/SessionService.php');
    require_once(APP_ROOT . '/module/Common/Database.php');
    require_once(APP_ROOT . '/module/Common/Logger.php');
?>

<?php
    if (!isLoggedIn()) {
        header('Location: ' . LOGIN_REGISTER_FORM);
        exit;
    }
?>

<?php
    require_once(APP_ROOT . '/module/Common/view/header.php')
?>

<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
    <?php
        require_once(APP_ROOT . '/module/Common/view/top-bar.php');
    ?>
    <div class="app-main">
        <?php
            $leftNavData = array('active_link' => 'Announcements');
            require_once(APP_ROOT . '/module/Common/view/left-nav.php');
        ?>

        <div class="app-main__outer">
            <div class="app-main__inner">
                <?php
                    $id = $_GET['id'];
                    try {
                        $getAnnouncement = 'SELECT *
                            FROM announcements
                            WHERE announcement_id = :announcement_id';
                        $stmt = $conn->prepare($getAnnouncement);
                        $stmt->bindParam(':announcement_id', $id);
                        $stmt->execute();

                        if ($stmt->rowCount() == 1) {
                            $announcement = $stmt->fetch(PDO::FETCH_ASSOC);
                        }
                    } catch (PDOException $e) {
                        logError($e->getMessage());
                    }
                ?>

                <?php
                    $pageContentHeaderData = array(
                        'icon' => 'pe-7s-display2',
                        'title' => 'Delete Announcement ' . $id . ': ' . $announcement['subject'],
                        'description' => 'This will delete the announcement.',
                        'features' => 'Non-recoverable',
                        'action_dropdown_options' => array(),
                        'back_button_href' => ANNOUNCEMENT_LIST,
                        'options' => array(
                            'show_action_dropdown' => false,
                            'show_back_button' => true
                        )
                    );
                    require_once(APP_ROOT . '/module/Common/view/page-content-header.php')
                ?>
                <div class="row">
                    <div class="col-md-12 overflow-auto mb-5">
                        <span class="d-block text-center text-danger font-size-lg font-weight-bold mb-4">
                            This will delete announcement "<?= $id; ?>: <?= $announcement['subject'] ?>" permanently.
                            Are you sure you want to continue?
                        </span>
                        <form method="post" action="<?=ANNOUNCEMENT_DELETE_PROCESS?>" name="delete_announcement" enctype="multipart/form-data">
                            <input type="hidden" name="id" value="<?= $id ?>">
                            <div class="position-relative form-group text-center">
                                <button class="btn btn-danger btn-sm margin-h-center" style="width: 100px;">Delete</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!--app-container-->

<?php
    require_once(APP_ROOT . '/module/Common/view/scripts.php')
?>

<?php
    require_once(APP_ROOT . '/module/Common/view/footer.php')
?>