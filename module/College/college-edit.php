<?php
    require_once('../../config/config.php');
    require_once('../../config/module-paths.php');
    require_once(APP_ROOT . '/module/Common/SessionService.php');
    require_once(APP_ROOT . '/module/Common/Database.php');
    require_once(APP_ROOT . '/module/Common/Logger.php');
    require_once(APP_ROOT . '/module/Common/Utilities.php')
?>

<?php
    if (!isLoggedIn()) {
        header('Location: ' . LOGIN_REGISTER_FORM);
        exit;
    }
?>

<?php
    require_once(APP_ROOT . '/module/Common/view/header.php')
?>

<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
    <?php
        require_once(APP_ROOT . '/module/Common/view/top-bar.php');
    ?>
    <div class="app-main">
        <?php
            $leftNavData = array('active_link' => 'Colleges');
            require_once(APP_ROOT . '/module/Common/view/left-nav.php');
        ?>

        <div class="app-main__outer">
            <div class="app-main__inner">
                <?php
                    $id = $_GET['id'];
                    try {
                        $getCollege = 'SELECT college_id, college_name FROM colleges WHERE college_id = :college_id';
                        $stmt = $conn->prepare($getCollege);
                        $stmt->bindParam(':college_id', $id);
                        $stmt->execute();

                        if ($stmt->rowCount() == 1) {
                            $college = $stmt->fetch(PDO::FETCH_ASSOC);
                        }
                    } catch (PDOException $e) {
                        logError($e->getMessage());
                    }
                ?>

                <?php
                    $pageContentHeaderData = array(
                        'icon' => 'pe-7s-display2',
                        'title' => 'Edit College ' . $id . ': ' . $college['college_name'],
                        'description' => 'Edit college details.',
                        'features' => 'Edit college\'s name.',
                        'action_dropdown_options' => array(),
                        'back_button_href' => COLLEGE_LIST,
                        'options' => array(
                            'show_action_dropdown' => false,
                            'show_back_button' => true
                        )
                    );
                    require_once(APP_ROOT . '/module/Common/view/page-content-header.php')
                ?>
                <div class="row">
                    <div class="col-md-12 overflow-auto mb-5">
                        <form method="post" action="<?=COLLEGE_EDIT_PROCESS?>" name="edit_college">
                            <div class="position-relative form-group">
                                <label for="college_name" class="font-weight-bold">College Name: </label>
                                <input class="form-control" type="text" id="college_name" name="college_name" value="<?= $college['college_name']; ?>" maxlength="255" required>
                            </div>
                            <input type="hidden" name="id" value="<?= $id ?>">
                            <div class="position-relative form-group">
                                <button class="form-control btn btn-primary btn-md float-left">Edit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!--app-container-->

<?php
    require_once(APP_ROOT . '/module/Common/view/scripts.php')
?>

<?php
    require_once(APP_ROOT . '/module/Common/view/footer.php')
?>