<?php
    require_once('../../config/config.php');
    require_once('../../config/module-paths.php');
    require_once(APP_ROOT . '/module/Common/SessionService.php');
    require_once(APP_ROOT . '/module/Common/Database.php');
    require_once(APP_ROOT . '/module/Common/Logger.php');

    if (!isLoggedIn()) {
        header('Location: ' . LOGIN_REGISTER_FORM);
        exit;
    }

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $id = isset($_POST['id']) ? $_POST['id'] : 0;
        $collegeName = isset($_POST['college_name']) ? $_POST['college_name'] : '';

        if (!empty($id) && !empty($collegeName)) {
            $editCollege = 'UPDATE colleges SET college_name = :college_name WHERE college_id = :college_id';

            $stmt = $conn->prepare($editCollege);
            $stmt->bindParam(':college_name', $collegeName);
            $stmt->bindParam(':college_id', $id);
            $stmt->execute();

            setNotificationSession('COLLEGE_EDIT', 'College Edited.');
            header('Location: ' . COLLEGE_EDIT . '?id=' . $id);
            exit;
        }
    } else {
        header('Location: ' . COLLEGE_LIST);
        exit;
    }