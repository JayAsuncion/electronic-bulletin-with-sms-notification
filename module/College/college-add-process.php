<?php
    require_once('../../config/config.php');
    require_once('../../config/module-paths.php');
    require_once(APP_ROOT . '/module/Common/SessionService.php');
    require_once(APP_ROOT . '/module/Common/Database.php');
    require_once(APP_ROOT . '/module/Common/Logger.php');
    require_once(APP_ROOT . '/module/Common/Utilities.php');

    if (!isLoggedIn()) {
        header('Location: ' . LOGIN_REGISTER_FORM);
        exit;
    }

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $collegeName = isset($_POST['college_name']) ? $_POST['college_name'] : '';

        if (!empty($collegeName)) {
            $addCollege = 'INSERT INTO colleges (college_name) VALUES(:college_name)';

            $stmt = $conn->prepare($addCollege);
            $stmt->bindParam(':college_name', $collegeName);
            $stmt->execute();

            setNotificationSession('COLLEGE_ADD', 'College Added.');
            header('Location: ' . COLLEGE_LIST);
            exit;
        }
    } else {
        header('Location: ' . COLLEGE_LIST);
        exit;
    }

