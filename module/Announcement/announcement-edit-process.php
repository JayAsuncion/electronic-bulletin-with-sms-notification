<?php
    require_once('../../config/config.php');
    require_once('../../config/module-paths.php');
    require_once(APP_ROOT . '/module/Common/SessionService.php');
    require_once(APP_ROOT . '/module/Common/Database.php');
    require_once(APP_ROOT . '/module/Common/Logger.php');

    if (!isLoggedIn()) {
        header('Location: ' . LOGIN_REGISTER_FORM);
        exit;
    }

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $id = isset($_POST['id']) ? $_POST['id'] : 0;
        $subject = isset($_POST['subject']) ? $_POST['subject'] : 0;
        $body = isset($_POST['body']) ? $_POST['body'] : 0;

        $userTypes = isset($_POST['user_types']) ? $_POST['user_types'] : 0;
        $colleges = isset($_POST['colleges']) ? $_POST['colleges'] : 0;
        $organizations = isset($_POST['organizations']) ? $_POST['organizations'] : 0;

        if (!empty($id) && !empty($subject) && !empty($body)) {
            $editAnnouncement = 'UPDATE announcements SET subject = :subject, body = :body WHERE announcement_id = :announcement_id';

            $stmt = $conn->prepare($editAnnouncement);
            $stmt->bindParam(':subject', $subject);
            $stmt->bindParam(':body', $body);
            $stmt->bindParam(':announcement_id', $id);
            $stmt->execute();

            $deleteQuery = 'DELETE FROM users_type_announcements
                WHERE announcement_id = :announcement_id';
            $stmt = $conn->prepare($deleteQuery);
            $stmt->bindParam(':announcement_id', $id);
            $stmt->execute();

            $deleteQuery = 'DELETE FROM colleges_announcements
                WHERE announcement_id = :announcement_id';
            $stmt = $conn->prepare($deleteQuery);
            $stmt->bindParam(':announcement_id', $id);
            $stmt->execute();

            $deleteQuery = 'DELETE FROM organizations_announcements
                WHERE announcement_id = :announcement_id';
            $stmt = $conn->prepare($deleteQuery);
            $stmt->bindParam(':announcement_id', $id);
            $stmt->execute();

            foreach ($userTypes as $userType) {
                $query = 'INSERT INTO users_type_announcements (announcement_id, users_type_id)
                    VALUES (:announcement_id, :users_type_id)';
                $stmt = $conn->prepare($query);
                $stmt->bindParam(':announcement_id', $id);
                $stmt->bindParam(':users_type_id', $userType);
                $stmt->execute();
            }

            foreach ($colleges as $college) {
                $query = 'INSERT INTO colleges_announcements (announcement_id, college_id)
                    VALUES (:announcement_id, :college_id)';
                $stmt = $conn->prepare($query);
                $stmt->bindParam(':announcement_id', $id);
                $stmt->bindParam(':college_id', $college);
                $stmt->execute();
            }

            foreach ($organizations as $organization) {
                $query = 'INSERT INTO organizations_announcements (announcement_id, organization_id)
                    VALUES (:announcement_id, :organization_id)';
                $stmt = $conn->prepare($query);
                $stmt->bindParam(':announcement_id', $id);
                $stmt->bindParam(':organization_id', $organization);
                $stmt->execute();
            }

            setNotificationSession('ANNOUNCEMENT_EDIT', 'Announcement Edited.');
            header('Location: ' . ANNOUNCEMENT_EDIT . '?id=' . $id);
            exit;
        }
    } else {
        header('Location: ' . ANNOUNCEMENT_LIST);
        exit;
    }