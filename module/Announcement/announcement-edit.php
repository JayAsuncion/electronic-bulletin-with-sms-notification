<?php
    require_once('../../config/config.php');
    require_once('../../config/module-paths.php');
    require_once(APP_ROOT . '/module/Common/SessionService.php');
    require_once(APP_ROOT . '/module/Common/Database.php');
    require_once(APP_ROOT . '/module/Common/Logger.php');
    require_once(APP_ROOT . '/module/Common/Utilities.php')
?>

<?php
    if (!isLoggedIn()) {
        header('Location: ' . LOGIN_REGISTER_FORM);
        exit;
    }

    $id = $_GET['id'];

    try {
        $getAnnouncement = 'SELECT announcements.*, CONCAT(first_name, " ", middle_name, " ", last_name) as "posted_by"
                                    FROM announcements
                                    INNER JOIN users_info ON announcements.posted_by_user_id = users_info.user_id
                                    WHERE announcement_id = :announcement_id';
        $stmt = $conn->prepare($getAnnouncement);
        $stmt->bindParam(':announcement_id', $id);
        $stmt->execute();

        if ($stmt->rowCount() == 1) {
            $announcement = $stmt->fetch(PDO::FETCH_ASSOC);
        }
    } catch (PDOException $e) {
        logError($e->getMessage());
    }

    $getUserTypesAnnouncement = 'SELECT id, announcement_id, users_type_id 
        FROM users_type_announcements WHERE announcement_id = :announcement_id';
    $stmt = $conn->prepare($getUserTypesAnnouncement);
    $stmt->bindParam(':announcement_id', $id);
    $stmt->execute();

    $userTypesAnnouncements = array();

    if ($stmt->rowCount() > 0) {
        $userTypesAnnouncements = $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    $getCollegesAnnouncement = 'SELECT id, announcement_id, college_id 
        FROM colleges_announcements WHERE announcement_id = :announcement_id';
    $stmt = $conn->prepare($getCollegesAnnouncement);
    $stmt->bindParam(':announcement_id', $id);
    $stmt->execute();

    $collegesAnnouncements = array();

    if ($stmt->rowCount() > 0) {
        $collegesAnnouncements = $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    $getOrganizationsAnnouncement = 'SELECT id, announcement_id, organization_id 
            FROM organizations_announcements WHERE announcement_id = :announcement_id';
    $stmt = $conn->prepare($getOrganizationsAnnouncement);
    $stmt->bindParam(':announcement_id', $id);
    $stmt->execute();

    $organizationsAnnouncements = array();

    if ($stmt->rowCount() > 0) {
        $organizationsAnnouncements = $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
?>

<?php
    require_once(APP_ROOT . '/module/Common/view/header.php')
?>

<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
    <?php
        require_once(APP_ROOT . '/module/Common/view/top-bar.php');
    ?>
    <div class="app-main">
        <?php
            $leftNavData = array('active_link' => 'Announcements');
            require_once(APP_ROOT . '/module/Common/view/left-nav.php');
        ?>

        <div class="app-main__outer">
            <div class="app-main__inner">
                <?php
                    $pageContentHeaderData = array(
                        'icon' => 'pe-7s-display2',
                        'title' => 'Edit Announcement ' . $id . ': ' . $announcement['subject'],
                        'description' => 'Edit announcement details.',
                        'features' => 'Edit announcement\'s subject, and body.',
                        'action_dropdown_options' => array(),
                        'back_button_href' => ANNOUNCEMENT_LIST,
                        'options' => array(
                            'show_action_dropdown' => false,
                            'show_back_button' => true
                        )
                    );
                    require_once(APP_ROOT . '/module/Common/view/page-content-header.php')
                ?>
                <div class="row">
                    <div class="col-md-12 overflow-auto mb-5">
                        <form method="post" action="<?=ANNOUNCEMENT_EDIT_PROCESS?>" name="edit_announcement" enctype="multipart/form-data">
                            <div class="position-relative form-group">
                                <label for="subject" class="font-weight-bold">Subject (<small class="font-weight-bold">Less than 255 characters</small>):</label>
                                <input class="form-control" type="text" id="subject" name="subject" value="<?= $announcement['subject']; ?>" maxlength="255" required>
                            </div>
                            <div class="position-relative form-group">
                                <label for="body" class="font-weight-bold">Body:</label>
                                <textarea class="form-control" id="body" name="body" rows="6" required><?= $announcement['body']; ?></textarea>
                            </div>
                            <div class="position-relative form-group">
                                <label for="date" class="font-weight-bold">Date & Time Posted: </label>
                                <span><?= $announcement['date']; ?></span> @ <span><?= get12HourTime($announcement['time']); ?></span>
                            </div>
                            <div class="position-relative form-group">
                                <label class="font-weight-bold pr-3 d-block">User Types:</label>
                                <?php
                                $getUserTypes = 'SELECT * FROM users_type';
                                $stmt = $conn->prepare($getUserTypes);
                                $stmt->execute();

                                if ($stmt->rowCount() > 0) {
                                    $userTypes = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    echo '<div class="position-relative form-group">';

                                    foreach ($userTypes as $userType) {
                                        $isCheckedFlag = '';
                                        foreach ($userTypesAnnouncements as $userTypesAnnouncement) {
                                            if ($userType['user_type_id'] == $userTypesAnnouncement['users_type_id']) {
                                                $isCheckedFlag = 'checked';
                                            }
                                        }

                                        echo '
                                                <div class="custom-checkbox custom-control custom-control-inline">
                                                    <input type="checkbox"
                                                        id="user_type_checkbox_' . $userType['user_type_id'] . '"
                                                        class="custom-control-input"
                                                        name="user_types[]"
                                                        value="' . $userType['user_type_id'] . '"
                                                        ' . $isCheckedFlag . '
                                                        >
                                                    <label class="custom-control-label"
                                                        for="user_type_checkbox_' . $userType['user_type_id'] . '">
                                                        ' . $userType['access_label'] . '
                                                    </label>
                                                </div>
                                            ';
                                    }

                                    echo '</div>';
                                }
                                ?>
                            </div>
                            <div class="position-relative form-group">
                                <label class="font-weight-bold pr-3 d-block">Colleges:</label>
                                <?php
                                $getColleges = 'SELECT * FROM colleges';
                                $stmt = $conn->prepare($getColleges);
                                $stmt->execute();

                                if ($stmt->rowCount() > 0) {
                                    $colleges = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    echo '<div class="position-relative form-group">';

                                    foreach ($colleges as $college) {
                                        $isCheckedFlag = '';
                                        foreach ($collegesAnnouncements as $collegesAnnouncement) {
                                            if ($college['college_id'] == $collegesAnnouncement['college_id']) {
                                                $isCheckedFlag = 'checked';
                                            }
                                        }

                                        echo '
                                            <div class="custom-checkbox custom-control custom-control-inline">
                                                <input type="checkbox"
                                                    id="college_checkbox_' . $college['college_id'] . '"
                                                    class="custom-control-input"
                                                    name="colleges[]"
                                                    value="' . $college['college_id'] . '"
                                                    ' . $isCheckedFlag . '>
                                                <label class="custom-control-label"
                                                    for="college_checkbox_' . $college['college_id'] . '">
                                                    ' . $college['college_name'] . '
                                                </label>
                                            </div>
                                        ';
                                    }

                                    echo '</div>';
                                }
                                ?>
                            </div>
                            <div class="position-relative form-group">
                                <label class="font-weight-bold pr-3 d-block">Organizations:</label>
                                <?php
                                $getOrganizations = 'SELECT * FROM organizations';
                                $stmt = $conn->prepare($getOrganizations);
                                $stmt->execute();

                                if ($stmt->rowCount() > 0) {
                                    $organizations = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    echo '<div class="position-relative form-group">';

                                    foreach ($organizations as $organization) {
                                        $isCheckedFlag = '';
                                        foreach ($organizationsAnnouncements as $organizationsAnnouncement) {
                                            if ($organization['organization_id'] == $organizationsAnnouncement['organization_id']) {
                                                $isCheckedFlag = 'checked';
                                            }
                                        }

                                        echo '
                                            <div class="custom-checkbox custom-control custom-control-inline">
                                                <input type="checkbox"
                                                    id="organization_checkbox_' . $organization['organization_id'] . '"
                                                    class="custom-control-input"
                                                    name="organizations[]"
                                                    value="' . $organization['organization_id'] . '"
                                                    ' . $isCheckedFlag . '>
                                                <label class="custom-control-label"
                                                    for="organization_checkbox_' . $organization['organization_id'] . '">
                                                    ' . $organization['organization_name'] . '
                                                </label>
                                            </div>
                                            ';
                                    }

                                    echo '</div>';
                                }
                                ?>
                            </div>
                            <input type="hidden" name="id" value="<?= $id ?>">
                            <div class="position-relative form-group">
                                <button class="form-control btn btn-primary btn-md float-left">Edit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!--app-container-->

<?php
    require_once(APP_ROOT . '/module/Common/view/scripts.php')
?>

<?php
    require_once(APP_ROOT . '/module/Common/view/footer.php')
?>