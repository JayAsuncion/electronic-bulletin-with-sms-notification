<?php
    require_once('../../config/config.php');
    require_once('../../config/module-paths.php');
    require_once(APP_ROOT . '/module/Common/SessionService.php');
    require_once(APP_ROOT . '/module/Common/Database.php');
    require_once(APP_ROOT . '/module/Common/Logger.php');
    require_once(APP_ROOT . '/module/Common/Utilities.php');

    if (!isLoggedIn()) {
        header('Location: ' . LOGIN_REGISTER_FORM);
        exit;
    }

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $subject = isset($_POST['subject']) ? $_POST['subject'] : 0;
        $body = isset($_POST['body']) ? $_POST['body'] : 0;
        $time = getCurrent24HourTime();
        $date = date('Y-m-d');
        $userID = getUserSession('user_id');

        $userTypes = isset($_POST['user_types']) ? $_POST['user_types'] : 0;
        $colleges = isset($_POST['colleges']) ? $_POST['colleges'] : 0;
        $organizations = isset($_POST['organizations']) ? $_POST['organizations'] : 0;

        if (!empty($subject) && !empty($body)) {
            $addEvent = 'INSERT INTO announcements(subject, body, date, time, posted_by_user_id)
                VALUES(:subject, :body, :date, :time, :posted_by_user_id) ';

            $stmt = $conn->prepare($addEvent);
            $stmt->bindParam(':subject', $subject);
            $stmt->bindParam(':body', $body);
            $stmt->bindParam(':date', $date);
            $stmt->bindParam(':time', $time);
            $stmt->bindParam(':posted_by_user_id', $userID);
            $stmt->execute();
            $announcementID = $conn->lastInsertId();

            foreach ($userTypes as $userType) {
                $query = 'INSERT INTO users_type_announcements (announcement_id, users_type_id)
                    VALUES (:announcement_id, :users_type_id)';
                $stmt = $conn->prepare($query);
                $stmt->bindParam(':announcement_id', $announcementID);
                $stmt->bindParam(':users_type_id', $userType);
                $stmt->execute();
            }

            foreach ($colleges as $college) {
                $query = 'INSERT INTO colleges_announcements (announcement_id, college_id)
                    VALUES (:announcement_id, :college_id)';
                $stmt = $conn->prepare($query);
                $stmt->bindParam(':announcement_id', $announcementID);
                $stmt->bindParam(':college_id', $college);
                $stmt->execute();
            }

            foreach ($organizations as $organization) {
                $query = 'INSERT INTO organizations_announcements (announcement_id, organization_id)
                    VALUES (:announcement_id, :organization_id)';
                $stmt = $conn->prepare($query);
                $stmt->bindParam(':announcement_id', $announcementID);
                $stmt->bindParam(':organization_id', $organization);
                $stmt->execute();
            }

            setNotificationSession('ANNOUNCEMENT_ADD', 'Announcement Added.');
            header('Location: ' . ANNOUNCEMENT_LIST);
            exit;
        }
    } else {
        header('Location: ' . ANNOUNCEMENT_LIST);
        exit;
    }

