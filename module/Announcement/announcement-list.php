<?php
    require_once('../../config/config.php');
    require_once('../../config/module-paths.php');
    require_once(APP_ROOT . '/module/Common/SessionService.php');
    require_once(APP_ROOT . '/module/Common/Database.php');
    require_once(APP_ROOT . '/module/Common/Logger.php');
    require_once(APP_ROOT . '/module/Common/Utilities.php');
?>

<?php
    if (!isLoggedIn()) {
        header('Location: ' . LOGIN_REGISTER_FORM);
        exit;
    }
?>

<?php
    require_once(APP_ROOT . '/module/Common/view/header.php')
?>

<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
    <?php
        require_once(APP_ROOT . '/module/Common/view/top-bar.php');
    ?>
    <div class="app-main">
        <?php
            $leftNavData = array('active_link' => 'Announcements');
            require_once(APP_ROOT . '/module/Common/view/left-nav.php');
        ?>

        <div class="app-main__outer">
            <div class="app-main__inner">
                <?php
                    $pageContentHeaderData = array(
                        'icon' => 'pe-7s-comment',
                        'title' => 'Announcements',
                        'description' => 'Manage announcement posts.',
                        'features' => 'Add/Edit/Delete Announcement Post',
                        'action_dropdown_options' => array(
                            array('href' => ANNOUNCEMENT_ADD, 'label' => 'Add Announcement', 'icon' => 'fa fa-plus fa-xs')
                        ),
                        'back_button_href' => ANNOUNCEMENT_LIST,
                        'options' => array(
                            'show_action_dropdown' => true,
                            'show_back_button' => false
                        )
                    );
                    require_once(APP_ROOT . '/module/Common/view/page-content-header.php')
                ?>
                <div class="row">
                    <div class="col-md-12 overflow-auto">
                        <table id="announcements_table" class="display table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="width: 30px;">ID</th>
                                    <th style="width: 125px;">Subject</th>
                                    <th>Body</th>
                                    <th style="width: 75px;">Date &amp; Time</th>
                                    <th style="width: 125px;">Posted By</th>
                                    <th style="width: 100px;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                try {
                                    $getAnnouncements = 'SELECT announcements.*, CONCAT(first_name, " ", middle_name, " ", last_name) as "posted_by"
                                        FROM announcements
                                        INNER JOIN users_info ON announcements.posted_by_user_id = users_info.user_id';
                                    $stmt = $conn->prepare($getAnnouncements);
                                    $stmt->execute();

                                    if ($stmt->rowCount() > 0) {
                                        $announcements = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                        foreach ($announcements as $announcement) {
                                            $announcement['body'] = strlen($announcement['body']) > 200 ?
                                                substr($announcement['body'], 0, 200) . "..."
                                                : $announcement['body'];

                                            echo '
                                            <tr>
                                                <td>' . $announcement['announcement_id'] . '</td>
                                                <td>' . $announcement['subject'] . '</td>
                                                <td>' . $announcement['body'] . '</td>
                                                <td>' . $announcement['date'] . '<br> @' . get12HourTime($announcement['time']) . '</td>
                                                <td>' . $announcement['posted_by'] . '</td>
                                                <td>
                                                <form class="d-inline-block mt-2 mb-2" action="'. ANNOUNCEMENT_EDIT .'" method="get">
                                                    <button class="btn btn-success btn-sm">Edit</button>
                                                    <input type="hidden" name="id" value="' . $announcement['announcement_id'] . '">
                                                </form>
                                                <form class="d-inline-block mt-2 mb-2" action="'. ANNOUNCEMENT_DELETE .'" method="get">
                                                    <button class="btn btn-danger btn-sm">Delete</button>
                                                    <input type="hidden" name="id" value="' . $announcement['announcement_id'] . '">
                                                </form>';

                                            if ($announcement['approved'] == '0') {
                                                echo '
                                                <form class="d-block mt-2 mb-2" action="'. ANNOUNCEMENT_APPROVE_PROCESS .'" method="post">
                                                    <button class="btn btn-alternate btn-sm" style="width: 100%">Approve</button>
                                                    <input type="hidden" name="id" value="' . $announcement['announcement_id'] . '">
                                                </form>
                                                ';
                                            } else if ($announcement['approved'] == '1') {
                                                echo '
                                                <form class="d-block mt-2 mb-2" action="'. ANNOUNCEMENT_DISAPPROVE_PROCESS .'" method="post">
                                                    <button class="btn btn-secondary btn-sm" style="width: 100%">Disapprove</button>
                                                    <input type="hidden" name="id" value="' . $announcement['announcement_id'] . '">
                                                </form>
                                                ';
                                            }
                                            echo '
                                                </td>
                                            </tr>
                                            ';
                                        }
                                    }
                                } catch (PDOException $e) {
                                    logError($e->getMessage());
                                }
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Subject</th>
                                    <th>Body</th>
                                    <th>Date &amp; Time</th>
                                    <th>Posted By</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!--app-container-->

<?php
    require_once(APP_ROOT . '/module/Common/view/scripts.php')
?>

<script>
    $(document).ready(function() {
        $('#announcements_table').DataTable( {
            "paging":   true,
            "ordering": true,
            "info":     false
        });
    });
</script>

<?php
    require_once(APP_ROOT . '/module/Common/view/footer.php')
?>