<?php
    require_once('../../config/config.php');
    require_once('../../config/module-paths.php');
    require_once(APP_ROOT . '/module/Common/SessionService.php');
    require_once(APP_ROOT . '/module/Common/Database.php');
    require_once(APP_ROOT . '/module/Common/Logger.php');

    if (!isLoggedIn()) {
        header('Location: ' . LOGIN_REGISTER_FORM);
        exit;
    }

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $id = isset($_POST['id']) ? $_POST['id'] : 0;

        if (!empty($id)) {
            $approveAnnouncement = 'UPDATE announcements SET approved = 0 WHERE announcement_id = :announcement_id';
            $stmt = $conn->prepare($approveAnnouncement);
            $stmt->bindParam(':announcement_id', $id);
            $stmt->execute();

            setNotificationSession('ANNOUNCEMENT_APPROVE', 'Announcement Approved.');
            header('Location: ' . ANNOUNCEMENT_LIST);
            exit;
        } else {
            header('Location: ' . ANNOUNCEMENT_LIST);
            exit;
        }
    } else {
        header('Location: ' . ANNOUNCEMENT_LIST);
        exit;
    }