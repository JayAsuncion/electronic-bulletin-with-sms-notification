<?php
    require_once('../../config/config.php');
    require_once('../../config/module-paths.php');
    require_once(APP_ROOT . '/module/Common/SessionService.php');
    require_once(APP_ROOT . '/module/Common/Database.php');
    require_once(APP_ROOT . '/module/Common/Logger.php');
    require_once(APP_ROOT . '/module/Common/Utilities.php')
?>

<?php
    if (!isLoggedIn()) {
        header('Location: ' . LOGIN_REGISTER_FORM);
        exit;
    }

    $id = isset($_GET['id']) ? $_GET['id'] : 0;

    if (!empty($id)) {
        $getUser = 'SELECT users_credentials.user_id, first_name, middle_name, last_name, phone_number, id_number, password,
            colleges.college_id, college_name, users_type.user_type_id, access_label 
            FROM users_credentials
            INNER JOIN users_info ON users_credentials.user_info_id = users_info.user_id
            INNER JOIN users_type ON users_credentials.user_type_id = users_type.user_type_id
            INNER JOIN colleges ON users_info.college_id = colleges.college_id
            WHERE users_info.user_id = :user_id';
        $stmt = $conn->prepare($getUser);
        $stmt->bindParam(':user_id', $id);
        $stmt->execute();

        if ($stmt->rowCount() == 0) {
            header('Location: ' . USER_LIST);
        }

        $user = $stmt->fetch(PDO::FETCH_ASSOC);
    }
?>

<?php
    require_once(APP_ROOT . '/module/Common/view/header.php')
?>

<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
    <?php
        require_once(APP_ROOT . '/module/Common/view/top-bar.php');
    ?>
    <div class="app-main">
        <?php
            $leftNavData = array('active_link' => 'Announcements');
            require_once(APP_ROOT . '/module/Common/view/left-nav.php');
        ?>

        <div class="app-main__outer">
            <div class="app-main__inner">
                <?php
                    $backButtonHref = !empty($user['user_type_id']) && !empty($user['college_id']) ?
                        USER_LIST . '?user_type_id=' . $user['user_type_id'] . '&college_id=' . $user['college_id'] :
                        USER_LIST;

                    $pageContentHeaderData = array(
                        'icon' => 'pe-7s-display2',
                        'title' => 'Edit User ' . $id . ': ' . $user['first_name'],
                        'description' => 'Edit user info.',
                        'features' => 'First name, middle name, last name, phone number, etc.',
                        'action_dropdown_options' => array(),
                        'back_button_href' => $backButtonHref,
                        'options' => array(
                            'show_action_dropdown' => false,
                            'show_back_button' => true
                        )
                    );
                    require_once(APP_ROOT . '/module/Common/view/page-content-header.php')
                ?>
                <div class="row">
                    <div class="col-md-12 overflow-auto mb-5">
                        <form method="post" action="<?=USER_EDIT_PROCESS?>" name="edit_user" enctype="multipart/form-data">
                            <div class="row mb-4">
                                <div class="position-relative form-group col-md-3">
                                    <label for="first_name" class="font-weight-bold">First Name</label>
                                    <input class="form-control" type="text" id="first_name" name="first_name" value="<?= $user['first_name']; ?>" maxlength="255" required>
                                </div>
                                <div class="position-relative  form-group col-md-3">
                                    <label for="middle_name" class="font-weight-bold">Middle Name</label>
                                    <input class="form-control" type="text" id="middle_name" name="middle_name" value="<?= $user['middle_name']; ?>" maxlength="255" required>
                                </div>
                                <div class="position-relative  form-group col-md-3">
                                    <label for="last_name" class="font-weight-bold">Last Name</label>
                                    <input class="form-control" type="text" id="last_name" name="last_name" value="<?= $user['last_name']; ?>" maxlength="255" required>
                                </div>
                                <div class="position-relative  form-group col-md-3">
                                    <label for="phone_number" class="font-weight-bold">Phone Number</label>
                                    <input class="form-control" type="number" id="phone_number" name="phone_number" value="<?= $user['phone_number']; ?>" maxlength="13" required>
                                </div>
                                <div class="position-relative  form-group col-md-2">
                                    <label for="id_number" class="font-weight-bold">ID Number</label>
                                    <input class="form-control" type="text" id="id_number" name="id_number" value="<?= $user['id_number']; ?>" maxlength="13" required>
                                </div>
                                <div class="position-relative  form-group col-md-2">
                                    <label for="password" class="font-weight-bold">Password</label>
                                    <input class="form-control" type="password" id="password" name="password" value="<?= $user['password']; ?>" maxlength="13" required>
                                </div>
                                <div class="position-relative  form-group col-md-8">
                                    <label for="college_id" class="font-weight-bold">College <?= $user['college_id']; ?></label>
                                    <select class="form-control" id="college_id" name="college_id">
                                        <?php
                                        $getColleges = 'SELECT * FROM colleges';
                                        $stmt = $conn->prepare($getColleges);
                                        $stmt->execute();

                                        if ($stmt->rowCount() > 0) {}
                                        $colleges = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                        foreach($colleges as $college) {
                                            if ($college['college_id'] == $user['college_id']) {
                                                echo '<option selected value="' . $college['college_id'] . '">' . $college['college_name'] . '</option>';
                                            } else {
                                                echo '<option value="' . $college['college_id'] . '">' . $college['college_name'] . '</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="position-relative  form-group col-md-3">
                                    <label for="college_id" class="font-weight-bold">User Type</label>
                                    <select class="form-control" id="user_type_id" name="user_type_id">
                                        <?php
                                        $getUserTypes = 'SELECT * FROM users_type';
                                        $stmt = $conn->prepare($getUserTypes);
                                        $stmt->execute();

                                        if ($stmt->rowCount() > 0) {
                                        }
                                        $userTypes = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                        foreach ($userTypes as $userType) {
                                            if ($userType['user_type_id'] == $user['user_type_id']) {
                                                echo '<option selected value="' . $userType['user_type_id'] . '">' . $userType['access_label'] . '</option>';
                                            } else {
                                                echo '<option value="' . $userType['user_type_id'] . '">' . $userType['access_label'] . '</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <input type="hidden" name="user_info_id" value="<?= $id; ?>">
                            </div>
                            <div class="position-relative form-group">
                                <button class="form-control btn btn-primary btn-md float-left">Edit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!--app-container-->

<?php
    require_once(APP_ROOT . '/module/Common/view/scripts.php')
?>

<?php
    require_once(APP_ROOT . '/module/Common/view/footer.php')
?>