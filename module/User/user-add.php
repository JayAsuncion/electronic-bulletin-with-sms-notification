<?php
    require_once('../../config/config.php');
    require_once('../../config/module-paths.php');
    require_once(APP_ROOT . '/module/Common/SessionService.php');
    require_once(APP_ROOT . '/module/Common/Database.php');
    require_once(APP_ROOT . '/module/Common/Logger.php');
?>

<?php
    if (!isLoggedIn()) {
        header('Location: ' . LOGIN_REGISTER_FORM);
        exit;
    }

    $userTypeID = isset($_GET['user_type_id']) ? $_GET['user_type_id'] : 0;
    $collegeID = isset($_GET['college_id']) ? $_GET['college_id'] : 0;

    $userTypeIDNotFoundFlag = false;
    $collegeIDNotFoundFlag = false;
    $collegeInfo = array();

    if (!empty($userTypeID) && !empty($collegeID)) {
        $getUserType = 'SELECT * FROM users_type WHERE user_type_id = :user_type_id';
        $stmt = $conn->prepare($getUserType);
        $stmt->bindParam(':user_type_id', $userTypeID);
        $stmt->execute();

        if ($stmt->rowCount() == 0) {
            $userTypeIDNotFoundFlag = true;
        } else {
            $userType = $stmt->fetch(PDO::FETCH_ASSOC);
        }

        $getCollege = 'SELECT * FROM colleges WHERE college_id = :college_id';
        $stmt = $conn->prepare($getCollege);
        $stmt->bindParam(':college_id', $collegeID);
        $stmt->execute();

        if ($stmt->rowCount() == 0) {
            $collegeIDNotFoundFlag = true;
        } else {
            $collegeInfo = $stmt->fetch(PDO::FETCH_ASSOC);
        }

        if ($userTypeIDNotFoundFlag) {
            setNotificationSession('INVALID_USER_TYPE', 'User type not found.');
        }

        if ($collegeIDNotFoundFlag) {
            setNotificationSession('INVALID_COLLEGE_ID', 'College not found.');
        }

        if ($userTypeIDNotFoundFlag || $collegeIDNotFoundFlag) {
            header('Location: ' . USER_LIST, true);
            exit;
        }
    } else {
        $userTypeIDNotFoundFlag = true;
        $collegeIDNotFoundFlag = true;
    }
?>

<?php
    require_once(APP_ROOT . '/module/Common/view/header.php')
?>

<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
    <?php
        require_once(APP_ROOT . '/module/Common/view/top-bar.php');
    ?>
    <div class="app-main">
        <?php
            $activeLink = $userTypeIDNotFoundFlag && $collegeIDNotFoundFlag ? 'Add User' : $userType['access_label'];
            $leftNavData = array('active_link' => $activeLink);
            require_once(APP_ROOT . '/module/Common/view/left-nav.php');
        ?>

        <div class="app-main__outer">
            <div class="app-main__inner">
                <?php
                    $backButtonHref = !empty($userTypeID) && !empty($collegeID) ?
                        USER_LIST . '?user_type_id=' . $userTypeID . '&college_id=' . $collegeID :
                        USER_LIST;

                    $pageContentHeaderData = array(
                        'icon' => 'pe-7s-display2',
                        'title' => 'Add User',
                        'description' => 'Add user to list of users',
                        'features' => 'Subject, and body.',
                        'action_dropdown_options' => array(),
                        'back_button_href' => $backButtonHref,
                        'options' => array(
                            'show_action_dropdown' => false,
                            'show_back_button' => !empty($userTypeID) && !empty($collegeID)
                        )
                    );
                    require_once(APP_ROOT . '/module/Common/view/page-content-header.php')
                ?>
                <div class="row">
                    <div class="col-md-12 overflow-auto mb-5">
                        <form method="post" action="<?=USER_ADD_PROCESS?>" name="add_user" enctype="multipart/form-data">
                            <div class="row mb-4">
                                <div class="position-relative form-group col-md-3">
                                    <label for="first_name" class="font-weight-bold">First Name</label>
                                    <input class="form-control" type="text" id="first_name" name="first_name" value="" maxlength="255" required>
                                </div>
                                <div class="position-relative  form-group col-md-3">
                                    <label for="middle_name" class="font-weight-bold">Middle Name</label>
                                    <input class="form-control" type="text" id="middle_name" name="middle_name" value="" maxlength="255" required>
                                </div>
                                <div class="position-relative  form-group col-md-3">
                                    <label for="last_name" class="font-weight-bold">Last Name</label>
                                    <input class="form-control" type="text" id="last_name" name="last_name" value="" maxlength="255" required>
                                </div>
                                <div class="position-relative  form-group col-md-3">
                                    <label for="phone_number" class="font-weight-bold">Phone Number</label>
                                    <input class="form-control" type="number" id="phone_number" name="phone_number" value="" maxlength="13" required>
                                </div>
                                <div class="position-relative  form-group col-md-2">
                                    <label for="id_number" class="font-weight-bold">ID Number</label>
                                    <input class="form-control" type="text" id="id_number" name="id_number" value="" maxlength="13" required>
                                </div>
                                <div class="position-relative  form-group col-md-2">
                                    <label for="password" class="font-weight-bold">Password</label>
                                    <input class="form-control" type="password" id="password" name="password" value="" maxlength="13" required>
                                </div>
                                <div class="position-relative  form-group col-md-8">
                                    <label for="college_id" class="font-weight-bold">College</label>
                                    <select class="form-control" id="college_id" name="college_id">
                                        <?php
                                        $getColleges = 'SELECT * FROM colleges';
                                        $stmt = $conn->prepare($getColleges);
                                        $stmt->execute();

                                        if ($stmt->rowCount() > 0) {}
                                            $colleges = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                            foreach($colleges as $college) {
                                                echo '<option value="' . $college['college_id'] . '">' . $college['college_name'] . '</option>';
                                            }
                                        ?>
                                    </select>
                                </div>
                                <?php
                                if (!empty($userTypeID)) {
                                    echo '<input type="hidden" name="user_type_id" value="' . $userTypeID . '" required>';
                                } else {
                                ?>
                                    <div class="position-relative  form-group col-md-3">
                                        <label for="college_id" class="font-weight-bold">User Type</label>
                                        <select class="form-control" id="user_type_id" name="user_type_id">
                                            <?php
                                            $getUserTypes = 'SELECT * FROM users_type';
                                            $stmt = $conn->prepare($getUserTypes);
                                            $stmt->execute();

                                            if ($stmt->rowCount() > 0) {
                                            }
                                            $userTypes = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                            foreach ($userTypes as $userType) {
                                                echo '<option value="' . $userType['user_type_id'] . '">' . $userType['access_label'] . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                <?php
                                }
                                ?>
                            </div>
                            <div class="position-relative form-group">
                                <button class="form-control btn btn-primary btn-md float-left">Add</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!--app-container-->

<?php
    require_once(APP_ROOT . '/module/Common/view/scripts.php')
?>

<?php
    require_once(APP_ROOT . '/module/Common/view/footer.php')
?>