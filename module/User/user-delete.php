<?php
    require_once('../../config/config.php');
    require_once('../../config/module-paths.php');
    require_once(APP_ROOT . '/module/Common/SessionService.php');
    require_once(APP_ROOT . '/module/Common/Database.php');
    require_once(APP_ROOT . '/module/Common/Logger.php');
?>

<?php
    if (!isLoggedIn()) {
        header('Location: ' . LOGIN_REGISTER_FORM);
        exit;
    }

    $id = isset($_GET['id']) ? $_GET['id'] : 0;

    if (!empty($id)) {
        $getUser = 'SELECT users_credentials.user_id, first_name, middle_name, last_name, phone_number, id_number, password,
            colleges.college_id, college_name, users_type.user_type_id, access_label 
            FROM users_credentials
            INNER JOIN users_info ON users_credentials.user_info_id = users_info.user_id
            INNER JOIN users_type ON users_credentials.user_type_id = users_type.user_type_id
            INNER JOIN colleges ON users_info.college_id = colleges.college_id
            WHERE users_info.user_id = :user_id';
        $stmt = $conn->prepare($getUser);
        $stmt->bindParam(':user_id', $id);
        $stmt->execute();

        if ($stmt->rowCount() == 0) {
            header('Location: ' . USER_LIST);
        }

        $user = $stmt->fetch(PDO::FETCH_ASSOC);
    }
?>

<?php
    require_once(APP_ROOT . '/module/Common/view/header.php')
?>

<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
    <?php
        require_once(APP_ROOT . '/module/Common/view/top-bar.php');
    ?>
    <div class="app-main">
        <?php
            $leftNavData = array('active_link' => 'Users');
            require_once(APP_ROOT . '/module/Common/view/left-nav.php');
        ?>

        <div class="app-main__outer">
            <div class="app-main__inner">
                <?php
                    $backButtonHref = !empty($user['user_type_id']) && !empty($user['college_id']) ?
                        USER_LIST . '?user_type_id=' . $user['user_type_id'] . '&college_id=' . $user['college_id'] :
                        USER_LIST;
                    $pageContentHeaderData = array(
                        'icon' => 'pe-7s-display2',
                        'title' => 'Delete User ' . $id . ': ' . $user['first_name'],
                        'description' => 'This will delete the user.',
                        'features' => 'Non-recoverable',
                        'action_dropdown_options' => array(),
                        'back_button_href' => $backButtonHref,
                        'options' => array(
                            'show_action_dropdown' => false,
                            'show_back_button' => true
                        )
                    );
                    require_once(APP_ROOT . '/module/Common/view/page-content-header.php')
                ?>
                <div class="row">
                    <div class="col-md-12 overflow-auto mb-5">
                        <span class="d-block text-center text-danger font-size-lg font-weight-bold mb-4">
                            This will delete user "<?= $id; ?>: <?= $user['first_name'] ?>" permanently.
                            Are you sure you want to continue?
                        </span>
                        <form method="post" action="<?=USER_DELETE_PROCESS?>" name="delete_user" enctype="multipart/form-data">
                            <input type="hidden" name="id" value="<?= $id ?>">
                            <div class="position-relative form-group text-center">
                                <button class="btn btn-danger btn-sm margin-h-center" style="width: 100px;">Delete</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!--app-container-->

<?php
    require_once(APP_ROOT . '/module/Common/view/scripts.php')
?>

<?php
    require_once(APP_ROOT . '/module/Common/view/footer.php')
?>