<?php
    require_once('../../config/config.php');
    require_once('../../config/module-paths.php');
    require_once(APP_ROOT . '/module/Common/SessionService.php');
    require_once(APP_ROOT . '/module/Common/Database.php');
    require_once(APP_ROOT . '/module/Common/Logger.php');

    if (!isLoggedIn()) {
        header('Location: ' . LOGIN_REGISTER_FORM);
        exit;
    }

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $id = isset($_POST['id']) ? $_POST['id'] : 0;

        if (!empty($id)) {
            $selectUser = 'SELECT users_credentials.user_type_id, users_info.college_id FROM users_info
            INNER JOIN users_credentials ON users_info.user_id = users_credentials.user_info_id
            WHERE users_info.user_id = :user_id';
            $stmt = $conn->prepare($selectUser);
            $stmt->bindParam(':user_id', $id);
            $stmt->execute();
            $redirectData = $stmt->fetch(PDO::FETCH_ASSOC);

            $deleteUserInfo = 'DELETE FROM users_info WHERE user_id = :user_id';
            $stmt = $conn->prepare($deleteUserInfo);
            $stmt->bindParam(':user_id', $id);
            $stmt->execute();

            $deleteUserCredentials = 'DELETE FROM users_credentials WHERE user_info_id = :user_id';
            $stmt = $conn->prepare($deleteUserCredentials);
            $stmt->bindParam(':user_id', $id);
            $stmt->execute();

            setNotificationSession('USER_DELETE', 'User Deleted.');
            header('Location: ' . USER_LIST . '?user_type_id=' . $redirectData['user_type_id'] . '&college_id=' . $redirectData['college_id']);
            exit;
        }
    } else {
        header('Location: ' . USER_LIST);
        exit;
    }