<?php
    require_once('../../config/config.php');
    require_once('../../config/module-paths.php');
    require_once(APP_ROOT . '/module/Common/SessionService.php');
    require_once(APP_ROOT . '/module/Common/Database.php');
    require_once(APP_ROOT . '/module/Common/Logger.php');
    require_once(APP_ROOT . '/module/Common/Utilities.php');
?>

<?php
    if (!isLoggedIn()) {
        header('Location: ' . LOGIN_REGISTER_FORM);
        exit;
    }

    $userTypeID = isset($_GET['user_type_id']) ? $_GET['user_type_id'] : 0;
    $collegeID = isset($_GET['college_id']) ? $_GET['college_id'] : 0;
    $userTypeIDNotFoundFlag = false;
    $collegeIDNotFoundFlag = false;


    $getUserType = 'SELECT * FROM users_type WHERE user_type_id = :user_type_id';
    $stmt = $conn->prepare($getUserType);
    $stmt->bindParam(':user_type_id', $userTypeID);
    $stmt->execute();

    $userType = array();

    if ($stmt->rowCount() == 0) {
        $userTypeIDNotFoundFlag = true;
    } else {
        $userType = $stmt->fetch(PDO::FETCH_ASSOC);
    }


    $getCollege = 'SELECT * FROM colleges WHERE college_id = :college_id';
    $stmt = $conn->prepare($getCollege);
    $stmt->bindParam(':college_id', $collegeID);
    $stmt->execute();

    $collegeInfo = array();

    if ($stmt->rowCount() == 0) {
        $collegeIDNotFoundFlag = true;
    } else {
        $collegeInfo = $stmt->fetch(PDO::FETCH_ASSOC);
    }

    if ($userTypeIDNotFoundFlag || $collegeIDNotFoundFlag) {
        $getFirstCollegeID = 'SELECT college_id FROM colleges LIMIT 1';
        $stmt = $conn->prepare($getFirstCollegeID);
        $stmt->execute();
        $firstCollegeID = $stmt->fetch(PDO::FETCH_ASSOC);
        $firstCollegeID = $firstCollegeID['college_id'];

        $getFirstUserType = 'SELECT user_type_id FROM users_type LIMIT 1';
        $stmt = $conn->prepare($getFirstUserType);
        $stmt->execute();
        $firstUserType = $stmt->fetch(PDO::FETCH_ASSOC);
        $firstUserType = $firstUserType['user_type_id'];

        header('Location: ' . USER_LIST . '?user_type_id=' . $firstUserType . '&college_id=' . $firstCollegeID);
        exit;
    }
?>

<?php
    require_once(APP_ROOT . '/module/Common/view/header.php')
?>

<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
    <?php
        require_once(APP_ROOT . '/module/Common/view/top-bar.php');
    ?>
    <div class="app-main">
        <?php
            $leftNavData = array('active_link' => $userType['access_label']);
            require_once(APP_ROOT . '/module/Common/view/left-nav.php');
        ?>

        <div class="app-main__outer">
            <div class="app-main__inner">
                <?php
                    $pageContentHeaderData = array(
                        'icon' => 'pe-7s-comment',
                        'title' => 'User List of ' . $userType['access_label'] . ' (' . $collegeInfo['college_name'] . ')',
                        'description' => 'Manage users.',
                        'features' => 'Add and Edit users information.',
                        'action_dropdown_options' => array(
                            array(
                                'href' => USER_ADD . '?user_type_id=' . $userTypeID . '&college_id=' . $collegeID, 'label' => 'Add User', 'icon' => 'fa fa-plus fa-xs')
                        ),
                        'back_button_href' => USER_LIST . '?user_type_id=' . $userTypeID . '&college_id=' . $collegeID,
                        'options' => array(
                            'show_action_dropdown' => true,
                            'show_back_button' => false
                        )
                    );
                    require_once(APP_ROOT . '/module/Common/view/page-content-header.php')
                ?>
                <div class="row">
                    <div class="col-md-12 overflow-auto">
                        <table id="users_table" class="display table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>First Name</th>
                                    <th>Middle Name</th>
                                    <th>Last Name</th>
                                    <th>Phone Number</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                try {
                                    $getUsersByCollegeID = 'SELECT users_info.*
                                    FROM users_info
                                    INNER JOIN users_credentials ON users_info.user_id = users_credentials.user_id
                                    INNER JOIN users_type ON users_credentials.user_type_id = users_type.user_type_id
                                    WHERE users_info.college_id = :college_id AND users_type.user_type_id = :user_type_id';
                                    $stmt = $conn->prepare($getUsersByCollegeID);
                                    $stmt->bindParam(':college_id', $collegeID);
                                    $stmt->bindParam(':user_type_id', $userTypeID);
                                    $stmt->execute();
                                } catch (PDOException $e) {
                                    logError($e->getMessage());
                                }
                                if ($stmt->rowCount() > 0) {
                                    $usersByCollegeID = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    foreach ($usersByCollegeID as $userByCollegeId) {
                                        echo '
                                            <tr>
                                                <td>' . $userByCollegeId['first_name'] . '</td>
                                                <td>' . $userByCollegeId['middle_name'] . '</td>
                                                <td>' . $userByCollegeId['last_name'] . '</td>
                                                <td>' . $userByCollegeId['phone_number'] . '</td>
                                                <td>
                                                <form class="d-inline-block mt-2 mb-2" action="'. USER_EDIT .'" method="get">
                                                    <button class="btn btn-success btn-sm">Edit</button>
                                                    <input type="hidden" name="id" value="' . $userByCollegeId['user_id'] . '">
                                                </form>
                                                <form class="d-inline-block mt-2 mb-2" action="'. USER_DELETE .'" method="get">
                                                    <button class="btn btn-danger btn-sm">Delete</button>
                                                    <input type="hidden" name="id" value="' . $userByCollegeId['user_id'] . '">
                                                </form>
                                                </td>
                                            </tr>
                                            ';
                                    }
                                }
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>First Name</th>
                                    <th>Middle Name</th>
                                    <th>Last Name</th>
                                    <th>Phone Number</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!--app-container-->

<?php
    require_once(APP_ROOT . '/module/Common/view/scripts.php')
?>

<script>
    $(document).ready(function() {
        $('#users_table').DataTable( {
            "paging":   true,
            "ordering": true,
            "info":     false
        });
    });
</script>

<?php
    require_once(APP_ROOT . '/module/Common/view/footer.php')
?>