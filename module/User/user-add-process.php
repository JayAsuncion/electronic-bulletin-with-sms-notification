<?php
    require_once('../../config/config.php');
    require_once('../../config/module-paths.php');
    require_once(APP_ROOT . '/module/Common/SessionService.php');
    require_once(APP_ROOT . '/module/Common/Database.php');
    require_once(APP_ROOT . '/module/Common/Logger.php');
    require_once(APP_ROOT . '/module/Common/Utilities.php');

    if (!isLoggedIn()) {
        header('Location: ' . LOGIN_REGISTER_FORM);
        exit;
    }

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $firstName = isset($_POST['first_name']) ? $_POST['first_name'] : '';
        $middleName = isset($_POST['middle_name']) ? $_POST['middle_name'] : '';
        $lastName = isset($_POST['last_name']) ? $_POST['last_name'] : '';
        $phoneNumber = isset($_POST['phone_number']) ? $_POST['phone_number'] : 0;
        $collegeID = isset($_POST['college_id']) ? $_POST['college_id'] : 0;
        $idNumber = isset($_POST['id_number']) ? $_POST['id_number'] : '';
        $password = isset($_POST['password']) ? $_POST['password'] : '';
        $userTypeID = isset($_POST['user_type_id']) ? $_POST['user_type_id'] : 0;

        if (
            !empty($firstName) && !empty($middleName) && !empty($lastName) &&
            !empty($phoneNumber) && !empty($collegeID) && !empty($idNumber) &&
            !empty($password) && !empty($userTypeID)
        ) {
            try {
                $addUserInfo = 'INSERT INTO users_info (first_name, middle_name, last_name, phone_number, college_id) 
                VALUES (:first_name, :middle_name, :last_name, :phone_number, :college_id)';
                $stmt = $conn->prepare($addUserInfo);
                $stmt->bindParam(':first_name', $firstName);
                $stmt->bindParam(':middle_name', $middleName);
                $stmt->bindParam(':last_name', $lastName);
                $stmt->bindParam(':phone_number', $phoneNumber);
                $stmt->bindParam(':college_id', $collegeID);
                $stmt->execute();

                $userInfoID = $conn->lastInsertId();

                $addUserCredentials = 'INSERT INTO users_credentials (id_number, password, user_type_id, user_info_id) 
                VALUES (:id_number, :password, :user_type_id, :user_info_id)';
                $stmt = $conn->prepare($addUserCredentials);
                $stmt->bindParam(':id_number', $idNumber);
                $stmt->bindParam(':password', $password);
                $stmt->bindParam(':user_type_id', $userTypeID);
                $stmt->bindParam(':user_info_id', $userInfoID);
                $stmt->execute();

                setNotificationSession('USER_ADD', 'User Added.');

                if (!empty($userTypeID) && !empty($collegeID)) {
                    header('Location: ' . USER_LIST . '?user_type_id=' . $userTypeID . '&college_id=' . $collegeID);
                } else {
                    header('Location: ' . EVENT_LIST);
                }
                exit;
            } catch (PDOException $e) {
                if ($e->getCode() == 23000) {
                    setNotificationSession('DUPLICATE_ID_NUMBER', 'ID Number already exists');
                    header('Location: ' . USER_ADD . '?user_type_id=' . $userTypeID . '&college_id=' . $collegeID);
                    exit;
                }
            }
        }
    } else {
        header('Location: ' . USER_LIST);
        exit;
    }

