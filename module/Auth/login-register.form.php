<?php
    require_once(APP_ROOT . '/module/Common/SessionService.php');
?>

<div class="container">
    <div class="row">
        <div class="col-md-4 offset-md-1">
            <div class="main-card mb-3 card bg-night-sky border border-white align-items-center">
                <div class="card-body">
                    <img class="img-fluid" src="<?=SITE_ROOT?>/img/logo2.png">
                </div>
            </div>
            <div class="main-card mb-3 card bg-night-sky border border-white align-items-center">
                <div class="card-body">
                    <h2 class="text-white text-center" style="font-family: serif">Electronic Bulletin with SMS Notification</h2>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="main-card mb-3 card border border-white" style="background-color: transparent; min-height: 300px">
                <div class="card-header text-white mt-3" style="background-color: transparent">
                    <i class="header-icon lnr-license icon-gradient bg-plum-plate"> </i><h4>Login/Register</h4>
                    <div class="btn-actions-pane-right">
                        <div class="nav">
                            <a data-toggle="tab" href="#tab-eg2-0"
                               class="btn-pill btn-wide active btn btn-outline-primary text-white btn-sm">Login</a>
<!--                            <a data-toggle="tab" href="#tab-eg2-1"-->
<!--                               class="btn-pill btn-wide mr-1 ml-1  btn btn-outline-primary text-white btn-sm">Register</a>-->
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-eg2-0" role="tabpanel">
                            <form method="post" action="<?=SITE_ROOT?>/module/Auth/verify-user.php" name="verify-user">
                                <div class="position-relative form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <label for="id_number_login" class="input-group-text bg-night-sky text-white">ID Number</label>
                                        </div>
                                        <input class="form-control" id="id_number_login" name="id_number" type="text" required>
                                        <div class="input-group-append">
                                            <label for="id_number_login" class="input-group-text bg-night-sky text-white"><li class="fa fa-user"></li></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="position-relative form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <label for="password_login" class="input-group-text bg-night-sky text-white" style="min-width: ">&nbsp Password </label>
                                        </div>
                                        <input class="form-control" id="password_login" name="password" type="password" required>
                                        <div class="input-group-append">
                                            <label for="password_login" class="input-group-text bg-night-sky text-white"><li class="fa fa-key"></li></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-5">
                                    <button class="btn btn-primary btn-md float-left" style="min-width: 150px;">Login</button>
                                </div>
                            </form>
                        </div>

<!--                        <div class="tab-pane" id="tab-eg2-1" role="tabpanel">-->
<!--                            <form method="post" action="--><?//=SITE_ROOT?><!--/module/User/create-user.php" name="create-user">-->
<!--                                <div class="position-relative form-group">-->
<!--                                    <div class="input-group">-->
<!--                                        <div class="input-group-prepend">-->
<!--                                            <label for="id_number_register" class="input-group-text bg-night-sky text-white">ID Number</label>-->
<!--                                        </div>-->
<!--                                        <input class="form-control" id="id_number_register" name="id_number" type="text" required>-->
<!--                                        <div class="input-group-append">-->
<!--                                            <label for="id_number_register" class="input-group-text bg-night-sky text-white"><li class="fa fa-user"></li></label>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="position-relative form-group">-->
<!--                                    <div class="input-group">-->
<!--                                        <div class="input-group-prepend">-->
<!--                                            <label for="password_register" class="input-group-text bg-night-sky text-white" style="min-width: ">&nbsp Password </label>-->
<!--                                        </div>-->
<!--                                        <input class="form-control" id="password_register" name="password" type="password" required>-->
<!--                                        <div class="input-group-append">-->
<!--                                            <label for="password_register" class="input-group-text bg-night-sky text-white"><li class="fa fa-key"></li></label>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="mt-5">-->
<!--                                    <button class="btn btn-primary btn-md float-right" style="min-width: 150px;">Register</button>-->
<!--                                </div>-->
<!--                            </form>-->
<!--                        </div>-->
                    </div>
                </div><!--card-body-->
            </div>
        </div>
    </div>
</div>