<?php
session_start();
require_once('../../config/config.php');
require_once('../../config/module-paths.php');
require_once(APP_ROOT . '/module/Common/SessionService.php');
require_once(APP_ROOT . '/module/Common/Database.php');
require_once(APP_ROOT . '/module/Common/Logger.php');

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $idNumber = isset($_POST['id_number']) ? $_POST['id_number'] : 0;
    $password = isset($_POST['password']) ? $_POST['password'] : 0;

    if (!empty($idNumber) && !empty($password)) {
        try {
            $getUserCredentials = 'SELECT users_credentials.user_id, id_number,
                first_name, middle_name, last_name,
                access_level, access_label,
                users_info.college_id, colleges.college_name
                FROM users_credentials
                INNER JOIN users_info ON users_credentials.user_info_id = users_info.user_id
                INNER JOIN users_type ON users_credentials.user_type_id = users_type.user_type_id
                INNER JOIN colleges ON users_info.college_id = colleges.college_id
                WHERE id_number = :id_number AND password = :password';
            $stmt = $conn->prepare($getUserCredentials);
            $stmt->bindParam(':id_number', $idNumber, PDO::PARAM_STR);
            $stmt->bindParam(':password', $password, PDO::PARAM_STR);
            $stmt->execute();

            if ($stmt->rowCount() == 1) {
                $user = $stmt->fetch(PDO::FETCH_ASSOC);
                setUserSession($user);
                echo "rowCount == 1<br>";
                switch ($user['access_level']) {
                    case 1:

                        break;
                    case 2:
                        setNotificationSession('LOGIN_SUCCESS', 'Successfully Logged-in!');
                        header('Location: ' . ANNOUNCEMENT_LIST);
                        break;
                    default:
                        logError('Invalid User Access Level for user ' . $user['id_number']);
                        break;
                }
            } else if ($stmt->rowCount() == 0) {
                setNotificationSession('NO_USER_FOUND', 'Invalid Credentials', 'warning');
                header('Location: ' . LOGIN_REGISTER_FORM);
            } else {
                $message = 'Multiple users matched. This should not happen. This is a database problem.';
                setNotificationSession('MULTIPLE_USERS_FOUND', 'Something went wrong. Check logs file.' , 'error');
                logError($message);
                header('Location: ' . LOGIN_REGISTER_FORM);
            }
        } catch (PDOException $e) {
            logError($e->getMessage());
        }
    } else {
        setNotificationSession('ID_NUMBER_PASSWORD_NOT_SET', 'ID Number and Password is required.', 'warning');
        header('Location: ' . LOGIN_REGISTER_FORM);
    }
}