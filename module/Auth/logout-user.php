<?php
require_once('../../config/config.php');
require_once('../../config/module-paths.php');
require_once(APP_ROOT . '/module/Common/SessionService.php');
require_once(APP_ROOT . '/module/Common/Database.php');
require_once(APP_ROOT . '/module/Common/Logger.php');

unsetUserSession();
header('Location: ' . LOGIN_REGISTER_FORM);
exit;