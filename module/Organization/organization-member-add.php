<?php
    require_once('../../config/config.php');
    require_once('../../config/module-paths.php');
    require_once(APP_ROOT . '/module/Common/SessionService.php');
    require_once(APP_ROOT . '/module/Common/Database.php');
    require_once(APP_ROOT . '/module/Common/Logger.php');
?>

<?php
    if (!isLoggedIn()) {
        header('Location: ' . LOGIN_REGISTER_FORM);
        exit;
    }

    $id = $_GET['id'];
    try {
        $getOrganization = 'SELECT organization_id, organization_name FROM organizations WHERE organization_id = :organization_id';
        $stmt = $conn->prepare($getOrganization);
        $stmt->bindParam(':organization_id', $id);
        $stmt->execute();

        if ($stmt->rowCount() == 1) {
            $organization = $stmt->fetch(PDO::FETCH_ASSOC);
        } else {
            setNotificationSession('INVALID_ORGANIZATION_ID', 'Organization not found.');
            header('Location: ' . ORGANIZATION_LIST);
            exit;
        }
    } catch (PDOException $e) {
        logError($e->getMessage());
    }
?>

<?php
    require_once(APP_ROOT . '/module/Common/view/header.php')
?>

<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
    <?php
        require_once(APP_ROOT . '/module/Common/view/top-bar.php');
    ?>
    <div class="app-main">
        <?php
            $leftNavData = array('active_link' => 'Organizations');
            require_once(APP_ROOT . '/module/Common/view/left-nav.php');
        ?>

        <div class="app-main__outer">
            <div class="app-main__inner">
                <?php
                    $pageContentHeaderData = array(
                        'icon' => 'pe-7s-display2',
                        'title' => 'Add Member (' . $organization['organization_name'] . ')',
                        'description' => 'Add to list of members.',
                        'features' => 'Organization Member',
                        'action_dropdown_options' => array(),
                        'back_button_href' => ORGANIZATION_MEMBER_LIST . '?id=' . $id,
                        'options' => array(
                            'show_action_dropdown' => false,
                            'show_back_button' => true
                        )
                    );
                    require_once(APP_ROOT . '/module/Common/view/page-content-header.php')
                ?>
                <div class="row">
                    <div class="col-md-12 overflow-auto mb-5">
                        <form method="post" action="<?=ORGANIZATION_MEMBER_ADD_PROCESS?>" name="add_organization_member">
                            <table id="table" class="display table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Name</th>
                                    <th style="max-width: 100px;">User Type</th>
                                    <th style="min-width: 100px;">College</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                try {
                                    $getUsers = 'SELECT users_credentials.user_id, first_name, middle_name, last_name,
                                    access_label, college_name
                                    FROM users_credentials
                                    INNER JOIN users_info ON users_credentials.user_info_id = users_info.user_id
                                    INNER JOIN users_type ON users_credentials.user_type_id = users_type.user_type_id
                                    INNER JOIN colleges ON users_info.college_id = colleges.college_id
                                    WHERE users_credentials.user_id NOT IN (
                                        SELECT user_credentials_id 
                                        FROM organizations_members
                                        WHERE organization_id = :organization_id
                                        )';
                                    $stmt = $conn->prepare($getUsers);
                                    $stmt->bindParam(':organization_id', $id);
                                    $stmt->execute();

                                    if ($stmt->rowCount() > 0) {
                                        $users = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                        foreach ($users as $user) {
                                            echo '
                                            <tr>
                                                <td><input type="checkbox" name="users[]" value="' . $user['user_id'] . '"></td>
                                                <td>'. $user['first_name'] . ' ' . $user['middle_name'] . ' ' . $user['last_name'] . '</td>
                                                <td>' . $user['access_label'] . '</td>
                                                <td>' . $user['college_name'] . '</td>
                                            </tr>
                                            ';
                                        }
                                    }
                                } catch (PDOException $e) {
                                    logError($e->getMessage());
                                }
                                ?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th></th>
                                    <th>Name</th>
                                    <th style="max-width: 100px;">User Type</th>
                                    <th style="min-width: 100px;">College</th>
                                </tr>
                                </tfoot>
                            </table>
                            <input type="hidden" name="organization_id" value="<?= $id; ?>">
                            <div class="position-relative form-group mt-4">
                                <button class="form-control btn btn-primary btn-md float-left">Add Member(s)</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!--app-container-->

<?php
    require_once(APP_ROOT . '/module/Common/view/scripts.php')
?>

<script>
    $(document).ready(function() {
        $('#table').DataTable( {
            "paging":   true,
            "ordering": true,
            "info":     false
        });
    });
</script>

<?php
    require_once(APP_ROOT . '/module/Common/view/footer.php')
?>