<?php
    require_once('../../config/config.php');
    require_once('../../config/module-paths.php');
    require_once(APP_ROOT . '/module/Common/SessionService.php');
    require_once(APP_ROOT . '/module/Common/Database.php');
    require_once(APP_ROOT . '/module/Common/Logger.php');
    require_once(APP_ROOT . '/module/Common/Utilities.php');

    if (!isLoggedIn()) {
        header('Location: ' . LOGIN_REGISTER_FORM);
        exit;
    }

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $organizationName = isset($_POST['organization_name']) ? $_POST['organization_name'] : '';

        if (!empty($organizationName)) {
            $addOrganization = 'INSERT INTO organizations (organization_name) VALUES(:organization_name)';

            $stmt = $conn->prepare($addOrganization);
            $stmt->bindParam(':organization_name', $organizationName);
            $stmt->execute();

            setNotificationSession('ORGANIZATION_ADD', 'Organization Added.');
            header('Location: ' . ORGANIZATION_LIST);
            exit;
        }
    } else {
        header('Location: ' . ORGANIZATION_LIST);
        exit;
    }

