<?php
    require_once('../../config/config.php');
    require_once('../../config/module-paths.php');
    require_once(APP_ROOT . '/module/Common/SessionService.php');
    require_once(APP_ROOT . '/module/Common/Database.php');
    require_once(APP_ROOT . '/module/Common/Logger.php');
    require_once(APP_ROOT . '/module/Common/Utilities.php');
?>

<?php
    if (!isLoggedIn()) {
        header('Location: ' . LOGIN_REGISTER_FORM);
        exit;
    }

    $id = $_GET['id'];
    try {
        $getOrganization = 'SELECT organization_id, organization_name FROM organizations WHERE organization_id = :organization_id';
        $stmt = $conn->prepare($getOrganization);
        $stmt->bindParam(':organization_id', $id);
        $stmt->execute();

        if ($stmt->rowCount() == 1) {
            $organization = $stmt->fetch(PDO::FETCH_ASSOC);
        } else {
            setNotificationSession('INVALID_ORGANIZATION_ID', 'Organization not found.');
            header('Location: ' . ORGANIZATION_LIST);
            exit;
        }
    } catch (PDOException $e) {
        logError($e->getMessage());
    }
?>

<?php
    require_once(APP_ROOT . '/module/Common/view/header.php')
?>

<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
    <?php
        require_once(APP_ROOT . '/module/Common/view/top-bar.php');
    ?>
    <div class="app-main">
        <?php
            $leftNavData = array('active_link' => 'Organizations');
            require_once(APP_ROOT . '/module/Common/view/left-nav.php');
        ?>

        <div class="app-main__outer">
            <div class="app-main__inner">
                <?php
                    $pageContentHeaderData = array(
                        'icon' => 'pe-7s-comment',
                        'title' => $organization['organization_name'] . ' Member List',
                        'description' => 'Manage organization members.',
                        'features' => 'Add/Delete Member',
                        'action_dropdown_options' => array(
                            array('href' => ORGANIZATION_MEMBER_ADD . '?id=' . $id, 'label' => 'Add Member', 'icon' => 'fa fa-plus fa-xs')
                        ),
                        'back_button_href' => ORGANIZATION_LIST,
                        'options' => array(
                            'show_action_dropdown' => true,
                            'show_back_button' => true
                        )
                    );
                    require_once(APP_ROOT . '/module/Common/view/page-content-header.php')
                ?>
                <div class="row">
                    <div class="col-md-12 overflow-auto">
                        <table id="announcements_table" class="display table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th style="max-width: 100px;">User Type</th>
                                    <th style="min-width: 100px;">College</th>
                                    <th style="max-width: 100px;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                try {
                                    $getOrganizationMembers = 'SELECT organization_member_id, first_name, middle_name, last_name,
                                    access_label, college_name
                                    FROM organizations_members
                                    INNER JOIN users_credentials ON organizations_members.user_credentials_id = users_credentials.user_id
                                    INNER JOIN users_info ON users_credentials.user_info_id = users_info.user_id
                                    INNER JOIN users_type ON users_credentials.user_type_id = users_type.user_type_id
                                    INNER JOIN colleges ON users_info.college_id = colleges.college_id
                                    WHERE organizations_members.organization_id = :organization_id';
                                    $stmt = $conn->prepare($getOrganizationMembers);
                                    $stmt->bindParam(':organization_id', $id);
                                    $stmt->execute();

                                    if ($stmt->rowCount() > 0) {
                                        $organizationsMembers = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                        foreach ($organizationsMembers as $organizationsMember) {
                                            echo '
                                            <tr>
                                                    <td>'. $organizationsMember['first_name'] . ' ' . $organizationsMember['middle_name'] . ' ' . $organizationsMember['last_name'] . '</td>
                                                    <td>' . $organizationsMember['access_label'] . '</td>
                                                    <td>' . $organizationsMember['college_name'] . '</td>
                                                    <td>
                                                    <form class="d-inline-block mt-2 mb-2" action="'. ORGANIZATION_MEMBER_DELETE .'" method="get">
                                                        <button class="btn btn-danger btn-sm" style="width: 100px;">Remove</button>
                                                        <input type="hidden" name="id" value="' . $organizationsMember['organization_member_id'] . '">
                                                        <input type="hidden" name="organization_id" value="' . $id . '">
                                                    </form>
                                                    </td>
                                            </tr>
                                            ';
                                        }
                                    }
                                } catch (PDOException $e) {
                                    logError($e->getMessage());
                                }
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Name</th>
                                    <th style="max-width: 100px;">User Type</th>
                                    <th style="min-width: 100px;">College</th>
                                    <th style="max-width: 100px;">Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!--app-container-->

<?php
    require_once(APP_ROOT . '/module/Common/view/scripts.php')
?>

<script>
    $(document).ready(function() {
        $('#announcements_table').DataTable( {
            "paging":   true,
            "ordering": true,
            "info":     false
        });
    });
</script>

<?php
    require_once(APP_ROOT . '/module/Common/view/footer.php')
?>