<?php
    require_once('../../config/config.php');
    require_once('../../config/module-paths.php');
    require_once(APP_ROOT . '/module/Common/SessionService.php');
    require_once(APP_ROOT . '/module/Common/Database.php');
    require_once(APP_ROOT . '/module/Common/Logger.php');
    require_once(APP_ROOT . '/module/Common/Utilities.php')
?>

<?php
    if (!isLoggedIn()) {
        header('Location: ' . LOGIN_REGISTER_FORM);
        exit;
    }

        $id = $_GET['id'];
        $organizationID = $_GET['organization_id'];

        try {
            $getOrganizationMember = 'SELECT first_name, middle_name, last_name FROM organizations_members
            INNER JOIN users_credentials ON organizations_members.user_credentials_id = users_credentials.user_id
            INNER JOIN users_info ON users_credentials.user_info_id = users_info.user_id
            WHERE organization_member_id = :organization_member_id';
            $stmt = $conn->prepare($getOrganizationMember);
            $stmt->bindParam(':organization_member_id', $id);
            $stmt->execute();

            if ($stmt->rowCount() == 1) {
                $member = $stmt->fetch(PDO::FETCH_ASSOC);
            } else {
                setNotificationSession('INVALID_ORGANIZATION_ID', 'Member not found.');
                header('Location: ' . ORGANIZATION_MEMBER_LIST . '?id=' . $organizationID);
                exit;
            }
        } catch (PDOException $e) {
            logError($e->getMessage());
        }
?>

<?php
    require_once(APP_ROOT . '/module/Common/view/header.php')
?>

<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
    <?php
        require_once(APP_ROOT . '/module/Common/view/top-bar.php');
    ?>
    <div class="app-main">
        <?php
            $leftNavData = array('active_link' => 'Organizations');
            require_once(APP_ROOT . '/module/Common/view/left-nav.php');
        ?>

        <div class="app-main__outer">
            <div class="app-main__inner">
                <?php
                    $pageContentHeaderData = array(
                        'icon' => 'pe-7s-display2',
                        'title' => 'Remove member ' . $id . ': ' . $member['first_name'] . ' ' . $member['middle_name'] . ' ' . $member['last_name'],
                        'description' => 'Remove member.',
                        'features' => 'This is permanent.',
                        'action_dropdown_options' => array(),
                        'back_button_href' => ORGANIZATION_MEMBER_LIST . '?id=' . $organizationID,
                        'options' => array(
                            'show_action_dropdown' => false,
                            'show_back_button' => true
                        )
                    );
                    require_once(APP_ROOT . '/module/Common/view/page-content-header.php')
                ?>
                <div class="row">
                    <div class="col-md-12 overflow-auto mb-5">
                        <span class="d-block text-center text-danger font-size-lg font-weight-bold mb-4">
                            This will remove member "<?= $id; ?>: <?= $member['first_name'] . ' ' . $member['middle_name'] . ' ' . $member['last_name'] ?>" permanently.
                            Are you sure you want to continue?
                        </span>
                        <form method="post" action="<?=ORGANIZATION_MEMBER_DELETE_PROCESS?>" name="delete_member" enctype="multipart/form-data">
                            <input type="hidden" name="id" value="<?= $id ?>">
                            <input type="hidden" name="organization_id" value="<?= $organizationID ?>">
                            <div class="position-relative form-group text-center">
                                <button class="btn btn-danger btn-sm margin-h-center" style="width: 100px;">Delete</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!--app-container-->

<?php
    require_once(APP_ROOT . '/module/Common/view/scripts.php')
?>

<?php
    require_once(APP_ROOT . '/module/Common/view/footer.php')
?>