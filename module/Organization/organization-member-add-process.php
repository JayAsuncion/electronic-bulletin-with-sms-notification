<?php
    require_once('../../config/config.php');
    require_once('../../config/module-paths.php');
    require_once(APP_ROOT . '/module/Common/SessionService.php');
    require_once(APP_ROOT . '/module/Common/Database.php');
    require_once(APP_ROOT . '/module/Common/Logger.php');
    require_once(APP_ROOT . '/module/Common/Utilities.php');

    if (!isLoggedIn()) {
        header('Location: ' . LOGIN_REGISTER_FORM);
        exit;
    }

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $users = isset($_POST['users']) ? $_POST['users'] : '';
        $organizationID = isset($_POST['organization_id']) ? $_POST['organization_id'] : 0;

        if (!empty($users) && !empty($organizationID)) {
            foreach ($users as $user) {
                $addMember = 'INSERT INTO organizations_members (user_credentials_id, organization_id)
                VALUES(:user_credentials_id, :organization_id)';
                $stmt = $conn->prepare($addMember);
                $stmt->bindParam(':user_credentials_id', $user, PDO::PARAM_INT);
                $stmt->bindParam(':organization_id', $organizationID, PDO::PARAM_INT);
                $stmt->execute();
            }


            setNotificationSession('ORGANIZATION_MEMBER_ADD', 'Member(s) Added.');
            header('Location: ' . ORGANIZATION_MEMBER_LIST . '?id=' . $organizationID);
            exit;
        }
    } else {
        header('Location: ' . ORGANIZATION_LIST);
        exit;
    }

