<?php
    require_once('../../config/config.php');
    require_once('../../config/module-paths.php');
    require_once(APP_ROOT . '/module/Common/SessionService.php');
    require_once(APP_ROOT . '/module/Common/Database.php');
    require_once(APP_ROOT . '/module/Common/Logger.php');

    if (!isLoggedIn()) {
        header('Location: ' . LOGIN_REGISTER_FORM);
        exit;
    }

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $id = isset($_POST['id']) ? $_POST['id'] : 0;
        $organizationName = isset($_POST['organization_name']) ? $_POST['organization_name'] : '';

        if (!empty($id) && !empty($organizationName)) {
            $editOrganization = 'UPDATE organizations SET organization_name = :organization_name WHERE organization_id = :organization_id';

            $stmt = $conn->prepare($editOrganization);
            $stmt->bindParam(':organization_name', $organizationName);
            $stmt->bindParam(':organization_id', $id);
            $stmt->execute();

            setNotificationSession('ORGANIZATION_EDIT', 'Organization Edited.');
            header('Location: ' . ORGANIZATION_EDIT . '?id=' . $id);
            exit;
        }
    } else {
        header('Location: ' . ORGANIZATION_LIST);
        exit;
    }