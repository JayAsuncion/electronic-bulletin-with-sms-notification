<?php
    require_once('../../config/config.php');
    require_once('../../config/module-paths.php');
    require_once(APP_ROOT . '/module/Common/SessionService.php');
    require_once(APP_ROOT . '/module/Common/Database.php');
    require_once(APP_ROOT . '/module/Common/Logger.php');
    require_once(APP_ROOT . '/module/Common/Utilities.php')
?>

<?php
    if (!isLoggedIn()) {
        header('Location: ' . LOGIN_REGISTER_FORM);
        exit;
    }

    $id = $_GET['id'];
    try {
        $getOrganization = 'SELECT organization_id, organization_name FROM organizations WHERE organization_id = :organization_id';
        $stmt = $conn->prepare($getOrganization);
        $stmt->bindParam(':organization_id', $id);
        $stmt->execute();

        if ($stmt->rowCount() == 1) {
            $organization = $stmt->fetch(PDO::FETCH_ASSOC);
        } else {
            setNotificationSession('INVALID_ORGANIZATION_ID', 'Organization not found.');
            header('Location: ' . ORGANIZATION_LIST);
            exit;
        }
    } catch (PDOException $e) {
        logError($e->getMessage());
    }
?>

<?php
    require_once(APP_ROOT . '/module/Common/view/header.php')
?>

<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
    <?php
        require_once(APP_ROOT . '/module/Common/view/top-bar.php');
    ?>
    <div class="app-main">
        <?php
            $leftNavData = array('active_link' => 'Organizations');
            require_once(APP_ROOT . '/module/Common/view/left-nav.php');
        ?>

        <div class="app-main__outer">
            <div class="app-main__inner">
                <?php
                    $pageContentHeaderData = array(
                        'icon' => 'pe-7s-display2',
                        'title' => 'Edit Organization ' . $id . ': ' . $organization['organization_name'],
                        'description' => 'Edit organization details.',
                        'features' => 'Edit organization\'s name.',
                        'action_dropdown_options' => array(),
                        'back_button_href' => ORGANIZATION_LIST,
                        'options' => array(
                            'show_action_dropdown' => false,
                            'show_back_button' => true
                        )
                    );
                    require_once(APP_ROOT . '/module/Common/view/page-content-header.php')
                ?>
                <div class="row">
                    <div class="col-md-12 overflow-auto mb-5">
                        <form method="post" action="<?=ORGANIZATION_EDIT_PROCESS?>" name="edit_organization">
                            <div class="position-relative form-group">
                                <label for="organization_name" class="font-weight-bold">Organization Name: </label>
                                <input class="form-control" type="text" id="organization_name" name="organization_name" value="<?= $organization['organization_name']; ?>" maxlength="255" required>
                            </div>
                            <input type="hidden" name="id" value="<?= $id ?>">
                            <div class="position-relative form-group">
                                <button class="form-control btn btn-primary btn-md float-left">Edit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!--app-container-->

<?php
    require_once(APP_ROOT . '/module/Common/view/scripts.php')
?>

<?php
    require_once(APP_ROOT . '/module/Common/view/footer.php')
?>