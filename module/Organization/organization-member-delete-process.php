<?php
    require_once('../../config/config.php');
    require_once('../../config/module-paths.php');
    require_once(APP_ROOT . '/module/Common/SessionService.php');
    require_once(APP_ROOT . '/module/Common/Database.php');
    require_once(APP_ROOT . '/module/Common/Logger.php');

    if (!isLoggedIn()) {
        header('Location: ' . LOGIN_REGISTER_FORM);
        exit;
    }

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $id = isset($_POST['id']) ? $_POST['id'] : 0;
        $organizationID = isset($_POST['organization_id']) ? $_POST['organization_id'] : 0;

        if (!empty($id)) {
            $deleteMember = 'DELETE FROM organizations_members WHERE organization_member_id = :organization_member_id';

            $stmt = $conn->prepare($deleteMember);
            $stmt->bindParam(':organization_member_id', $id);
            $stmt->execute();

            setNotificationSession('ORGANIZATION_MEMBER_DELETE', 'Member Removed.');
            header('Location: ' . ORGANIZATION_MEMBER_LIST . '?id=' . $organizationID);
            exit;
        }
    } else {
        header('Location: ' . ORGANIZATION_LIST);
        exit;
    }