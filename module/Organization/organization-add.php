<?php
    require_once('../../config/config.php');
    require_once('../../config/module-paths.php');
    require_once(APP_ROOT . '/module/Common/SessionService.php');
    require_once(APP_ROOT . '/module/Common/Database.php');
    require_once(APP_ROOT . '/module/Common/Logger.php');
?>

<?php
    if (!isLoggedIn()) {
        header('Location: ' . LOGIN_REGISTER_FORM);
        exit;
    }
?>

<?php
    require_once(APP_ROOT . '/module/Common/view/header.php')
?>

<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
    <?php
        require_once(APP_ROOT . '/module/Common/view/top-bar.php');
    ?>
    <div class="app-main">
        <?php
            $leftNavData = array('active_link' => 'Organizations');
            require_once(APP_ROOT . '/module/Common/view/left-nav.php');
        ?>

        <div class="app-main__outer">
            <div class="app-main__inner">
                <?php
                    $pageContentHeaderData = array(
                        'icon' => 'pe-7s-display2',
                        'title' => 'Add Organization',
                        'description' => 'Add to list of organizations.',
                        'features' => 'Organization Name',
                        'action_dropdown_options' => array(),
                        'back_button_href' => ORGANIZATION_LIST,
                        'options' => array(
                            'show_action_dropdown' => false,
                            'show_back_button' => true
                        )
                    );
                    require_once(APP_ROOT . '/module/Common/view/page-content-header.php')
                ?>
                <div class="row">
                    <div class="col-md-12 overflow-auto mb-5">
                        <form method="post" action="<?=ORGANIZATION_ADD_PROCESS?>" name="add_organization">
                            <div class="position-relative form-group">
                                <label for="organization_name" class="font-weight-bold">Organization Name :</label>
                                <input class="form-control" type="text" id="organization_name" name="organization_name" value="" maxlength="255" required>
                            </div>
                            <div class="position-relative form-group">
                                <button class="form-control btn btn-primary btn-md float-left">Add</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!--app-container-->

<?php
    require_once(APP_ROOT . '/module/Common/view/scripts.php')
?>

<?php
    require_once(APP_ROOT . '/module/Common/view/footer.php')
?>