<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?= SITE_NAME ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
    <meta name="description" content="Electronic bulletin website with SMS Notification">
    <link href="<?=SITE_ROOT?>/css/theme/architect-ui-free.css" rel="stylesheet">
    <link href="<?=SITE_ROOT?>/plugin/font-awesome/css/all.css" rel="stylesheet">
    <link href="<?=SITE_ROOT?>/plugin/data-tables/datatables.min.css" rel="stylesheet">
</head>
<body>