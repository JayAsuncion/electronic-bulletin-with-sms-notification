<div class="app-sidebar sidebar-shadow">
    <div class="app-header__logo">
        <div class="logo-src"></div>
        <div class="header__pane ml-auto">
            <div>
                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
    <div class="app-header__mobile-menu">
        <div>
            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
        </div>
    </div>
    <div class="app-header__menu">
        <span>
            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                <span class="btn-icon-wrapper">
                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                </span>
            </button>
        </span>
    </div>
    <div class="scrollbar-sidebar">
        <div class="app-sidebar__inner">
            <ul class="vertical-nav-menu">
                <li class="app-sidebar__heading">Electronic Bulletin</li>
                <li <?= $leftNavData['active_link'] == 'Announcements' ? 'class="mm-active"' : ''?>>
                    <a href="<?= ANNOUNCEMENT_LIST; ?>">
                        <i class="metismenu-icon fa fa-bullhorn"></i>
                        Announcements
                    </a>
                </li>
                <li <?= $leftNavData['active_link'] == 'Events' ? 'class="mm-active"' : ''?>>
                    <a href="<?= EVENT_LIST; ?>">
                        <i class="metismenu-icon fa fa-calendar-alt "></i>
                        Events
                    </a>
                </li>
                <li class="app-sidebar__heading">Users</li>
                <?php
                try {
                    $getUsersAndColleges = '
                    SELECT users_type.user_type_id, users_type.access_label, users_type.icon,
                           colleges.college_id, colleges.college_name
                    FROM users_type
                    INNER JOIN users_credentials ON users_type.user_type_id = users_credentials.user_type_id
                    INNER JOIN users_info ON users_credentials.user_info_id = users_info.user_id
                    INNER JOIN colleges ON users_info.college_id = colleges.college_id
                    GROUP BY users_type.user_type_id, users_info.college_id
                    ORDER BY users_type.user_type_id';
                    $stmt = $conn->prepare($getUsersAndColleges);
                    $stmt->execute();

                    if ($stmt->rowCount() > 0) {
                        $usersTypesAndColleges = $stmt->fetchAll(PDO::FETCH_ASSOC);
                        $currentUserTypeID = '';
                        $newUserTypeFlag = true;

                        foreach($usersTypesAndColleges as $data) {
                            if ($currentUserTypeID != $data['user_type_id']) {
                                if (!$newUserTypeFlag) {
                                    echo '
                                        </ul>
                                    </li>';
                                    $newUserTypeFlag = true;
                                }

                                if ($newUserTypeFlag) {
                                    $status = $leftNavData['active_link'] == $data['access_label'] ? 'class="mm-active"' : '';
                                    echo '
                                    <li ' . $status . '>
                                        <a href="' . USER_LIST . '">
                                            <i class="metismenu-icon ' . $data['icon'] . '"></i>
                                            ' . $data['access_label'] . '
                                            <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                        </a>
                                        <ul>';
                                }

                                $newUserTypeFlag = false;
                                $currentUserTypeID = $data['user_type_id'];
                            }

                            echo '
                            <form action="' . USER_LIST . '" method="get">
                            <input type="hidden" name="user_type_id" value="' . $data['user_type_id'] . '">
                            <input type="hidden" name="college_id" value="' . $data['college_id'] . '">
                            <button class="btn btn-outline-light btn-block" href="#" type="submit">
                                <i class="metismenu-icon"></i>
                                ' . $data['college_name'] . '
                            </button>
                            </form>';
                        }

                        echo '
                            </ul>
                        </li>';
                    }
                } catch (PDOException $e) {
                    logError($e->getMessage());
                }
                ?>
                <li <?= $leftNavData['active_link'] == 'Add User' ? 'class="mm-active"' : ''?>>
                    <a href="<?= USER_ADD; ?>">
                        <i class="metismenu-icon fa fa-user-plus"></i>
                        Add User
                    </a>
                </li>
                <li class="app-sidebar__heading">Colleges & Organizations</li>
                <li <?= $leftNavData['active_link'] == 'Colleges' ? 'class="mm-active"' : ''?>>
                    <a href="<?= COLLEGE_LIST; ?>">
                        <i class="metismenu-icon fa fa-university"></i>
                        Colleges
                    </a>
                </li>
                <li <?= $leftNavData['active_link'] == 'Organizations' ? 'class="mm-active"' : ''?>>
                    <a href="<?= ORGANIZATION_LIST; ?>">
                        <i class="metismenu-icon fa fa-users"></i>
                        Organizations
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>