<?php
    require_once('../../../config/config.php');
?>

<?php
    require_once(APP_ROOT . '/module/Common/view/header.php')
?>

<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
    <?php
        require_once(APP_ROOT . '/module/Common/view/top-bar.php');
    ?>
    <?php
        //require_once(APP_ROOT . '/module/Common/view/sidebar.php');
    ?>
    <div class="app-main">
        <?php
            require_once(APP_ROOT . '/module/Common/view/left-nav.php');
        ?>

        <?php
            //require_once(APP_ROOT . '/module/Student/student-list.php');
        ?>
    </div>
</div> <!--app-container-->

<?php
require_once('module/Common/view/scripts.php')
?>

<script>

</script>

<?php
require_once('module/Common/view/footer.php')
?>