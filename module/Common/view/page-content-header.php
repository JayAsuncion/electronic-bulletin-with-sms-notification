<div class="app-page-title">
    <?php
    if ($pageContentHeaderData['options']['show_back_button']) {
        echo '
        <div class="mb-4">
            <a class="btn btn-outline-primary" href="' . $pageContentHeaderData['back_button_href'] . '" style="min-width: 100px;">
                <span class="fa fa-less-than fa-sm" style="padding-right: 5px"></span>
                Back
            </a>
        </div>
        ';
    }
    ?>
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div class="page-title-icon">
                <i class="<?= $pageContentHeaderData['icon']; ?> icon-gradient bg-tempting-azure">
                </i>
            </div>
            <div><?= $pageContentHeaderData['title']; ?>
                <div class="page-title-subheading">
                    <?= $pageContentHeaderData['description'] ?> <small>(<?= $pageContentHeaderData['features']; ?>)</small>
                </div>
            </div>
        </div>
    <?php
    if ($pageContentHeaderData['options']['show_action_dropdown'] && !empty($pageContentHeaderData['action_dropdown_options'])) {
        echo '
        <div class="page-title-actions">
            <div class="d-inline-block dropdown">
                <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn-shadow dropdown-toggle btn btn-info">
                        <span class="btn-icon-wrapper pr-2 opacity-7">
                            <i class="fa fa-list-ul fa-w-20"></i>
                        </span>
                    Actions
                </button>
                <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                    <ul class="nav flex-column">';

                    foreach($pageContentHeaderData['action_dropdown_options'] as $actionDropDownOption) {
                        echo '
                        <li class="nav-item">
                            <a href="' . $actionDropDownOption['href'] . '" class="nav-link">
                                <span class="pl-4">
                                    ' . $actionDropDownOption['label'] . '
                                </span>
                                <div class="ml-auto badge badge-pill badge-primary"><i class="' . $actionDropDownOption['icon'] . '"></i></div>
                            </a>
                        </li>';
                    }
        echo         '</ul>
                </div>
            </div>
        </div>
        ';
    }
    ?>
    </div>
</div> <!-- Header -->