<?php
    $notifications = getNotificationSession();

    if (($notifications) > 0) {
        foreach ($notifications as $notification) {
            alertNotification($notification);
        }
        unsetNotificationSession();
    }
?>
</body>
</html>