<?php
function logError($message)
{
    $debugBackTrace = debug_backtrace();
    $timestamp = time();
    $time = date('H:i:s A', $timestamp);

    file_put_contents(
        APP_ROOT. '/logs/log_'.date("j.n.Y").'.log',
        $time . ' - ' . $debugBackTrace . ' >>> ' . $message . PHP_EOL,
        FILE_APPEND
    );
}