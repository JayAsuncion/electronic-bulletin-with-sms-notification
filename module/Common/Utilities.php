<?php
function getCurrent24HourTime()
{
    $timestamp = time();
    $time = date('H:i:s', $timestamp);
    return $time;
}

function get12HourTime($time)
{
    return date('h:i:s A', strtotime($time));
}