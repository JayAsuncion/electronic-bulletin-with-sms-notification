<?php
session_start();
if (!isset($_SESSION['notifications'])) {
    $_SESSION['notifications']= array();
}


function isLoggedIn()
{
    if (
        isset($_SESSION['current_user']['id_number']) &&
        isset($_SESSION['current_user']['access_level'])
    ) {
        return true;
    }

    return false;
}

function getUserSession($index = '')
{
    if (empty($index) && $index !== 0) {
        return $_SESSION['current_user'];
    } else {
        return $_SESSION['current_user'][$index];
    }
}

function setUserSession($userDataArray)
{
    $_SESSION['current_user']['user_id'] = $userDataArray['user_id'];
    $_SESSION['current_user']['id_number'] = $userDataArray['id_number'];
    $_SESSION['current_user']['access_level'] = $userDataArray['access_level'];
    $_SESSION['current_user']['access_label'] = $userDataArray['access_label'];
    $_SESSION['current_user']['first_name'] = $userDataArray['first_name'];
    $_SESSION['current_user']['middle_name'] = $userDataArray['middle_name'];
    $_SESSION['current_user']['last_name'] = $userDataArray['last_name'];
    $_SESSION['current_user']['college_id'] = $userDataArray['college_id'];
    $_SESSION['current_user']['college_name'] = $userDataArray['college_name'];
}

function unsetUserSession()
{
    unset($_SESSION['current_user']);
}

function getNotificationSession($index = '')
{
    if (!isset($_SESSION['notifications'])) {
        return 0;
    }

    if (empty($index) && $index !== 0) {
        return $_SESSION['notifications'];
    } else {
        return $_SESSION['notifications'][$index];
    }
}
function setNotificationSession($title, $message, $type = 'success')
{
    $notifications = array('title' => $title, 'message' => $message, 'type' => $type);
    array_push($_SESSION['notifications'], $notifications);
}

function unsetNotificationSession($index = '')
{
    if (empty($index) && $index !== 0) {
        unset($_SESSION['notifications']);
    } else {
        unset($_SESSION['notifications'][$index]);
    }
}

function renderToastRNotification($notification)
{
    echo '
        <div class="toast bg-danger error-toast toast-custom-style" id="myToast" data-delay="3000">
            <div class="toast-header">
                <strong class="mr-auto"><i class="fa fa-times"></i> Notification!</strong>
                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast">&times;</button>
            </div>
            <div class="toast-body bg-white">
            ' . $notification['message'] . '
            </div>
        </div>';
}

function renderHTMLAlertNotification($notification)
{
    echo '<div class="alert alert-' . $notification['type'] . '" role="alert">' . $notification['message'] . '</div>';
}

function alertNotification($notification)
{
    echo '<script>$(document).ready(function() {alert("' . $notification['message'] . '")});</script>';
}

function unsetAllSession()
{
    $_SESSION = array();
    session_destroy();
}