<?php
function uploadImage($subject)
{
    try{
        if (!isset($_FILES['image']['error']) || is_array($_FILES['image']['error'])
        ) {
            throw new RuntimeException('Invalid parameters.');
        }

        switch ($_FILES['image']['error']) {
            case UPLOAD_ERR_OK: //Value: 0; There is no error, the file uploaded with success.
                break;
            case UPLOAD_ERR_INI_SIZE: //Value: 1; The uploaded file exceeds the upload_max_filesize directive in php.ini.
                throw new RuntimeException('Exceeded filesize limit in php.ini.');
            case UPLOAD_ERR_FORM_SIZE: //Value: 2; The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.
                throw new RuntimeException('Exceeded filesize limit in HTML Form.');
            case UPLOAD_ERR_PARTIAL: //Value: 3; The uploaded file was only partially uploaded.
                throw new RuntimeException('File Uploaded Partially');
            case UPLOAD_ERR_NO_FILE: //Value: 4; No file was uploaded.
                throw new RuntimeException('No file sent.');
            case UPLOAD_ERR_NO_TMP_DIR:
                throw new RuntimeException('Missing a temporary folder'); //Value: 6; Missing a temporary folder.
            case UPLOAD_ERR_CANT_WRITE:
                throw new RuntimeException('Failed to write file to disk'); //Value: 7; Failed to write file to disk.
            default:
                throw new RuntimeException('Unknown errors.');
        }

        if(isset($_FILES['image'])) { //CHECK IF FILE EXISTS
            $errors     = array();
            $maxsize    = 8048000; //8 MB
            $acceptable = array(
                //'application/pdf',
                'image/jpeg',
                'image/jpg',
                'image/gif',
                'image/png'
            );

            if(($_FILES['image']['size'] >= $maxsize)){
                $errors[] = 'File too large. File must be less than 8 megabytes.';
            }
            if(($_FILES["image"]["size"] == 0)){
                $errors[] = 'File has 0 file size';
            }

            if((!in_array($_FILES['image']['type'], $acceptable)) && (!empty($_FILES["image"]["type"]))) {
                $errors[] = 'Invalid file type. Only PDF, JPG, GIF and PNG types are accepted.';
            }

            $path = $_FILES['image']['name'];
            $ext = pathinfo($path, PATHINFO_EXTENSION);//get extension of file
            $timestamp = time(); // get Unix Time stamp
            $relative_path = "/img/events/".$subject.'_'.$timestamp.".".$ext;
            $upload_path = APP_ROOT . $relative_path;

            if(count($errors) === 0) {
                move_uploaded_file($_FILES['image']['tmp_name'], $upload_path);
                return $relative_path;
            } else {
                foreach($errors as $error) {
                    logError($error);
                    setNotificationSession('IMAGE_UPLOAD_ERROR', $error);
                }
                return 0;
            }
        }

        logError('$_FILES[\'image\'] not isset.');
        return 0;
    }catch(RuntimeException $e){
        logError($e->getMessage());
        return 0;
    }
}