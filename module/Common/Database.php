<?php
require_once(APP_ROOT . '/config/config.php');
require_once(APP_ROOT . '/module/Common/Logger.php');

$dsn = 'mysql:host=' . DB_HOST . ';dbname=' . DB_NAME;
$options = array(
    PDO::ATTR_PERSISTENT => true,
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
);

try {
    $conn = new PDO($dsn, DB_USER, DB_PASS, $options);
} catch(PDOException $e) {
    $errorMessage = $e->getMessage();
    logError($errorMessage);
    setNotificationSession('DATABASE_ERROR', 'There was something wrong with you Database Connection. Kindly check');
    header('Location: ' . LOGIN_REGISTER_FORM);
}