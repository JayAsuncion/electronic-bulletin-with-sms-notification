<?php
    require_once('../../config/config.php');
    require_once('../../config/module-paths.php');
    require_once(APP_ROOT . '/module/Common/SessionService.php');
    require_once(APP_ROOT . '/module/Common/Database.php');
    require_once(APP_ROOT . '/module/Common/Logger.php');
    require_once(APP_ROOT . '/module/Common/FileUploader.php');
    require_once(APP_ROOT . '/module/Common/Utilities.php');

    if (!isLoggedIn()) {
        header('Location: ' . LOGIN_REGISTER_FORM);
        exit;
    }

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $subject = isset($_POST['subject']) ? $_POST['subject'] : 0;
        $body = isset($_POST['body']) ? $_POST['body'] : 0;
        $time = getCurrent24HourTime();
        $date = date('Y-m-d');
        $userID = getUserSession('user_id');

        $userTypes = isset($_POST['user_types']) ? $_POST['user_types'] : 0;
        $colleges = isset($_POST['colleges']) ? $_POST['colleges'] : 0;
        $organizations = isset($_POST['organizations']) ? $_POST['organizations'] : 0;

        if (!empty($subject) && !empty($body)) {
            $addEvent = 'INSERT INTO events(subject, body, date, time, posted_by_user_id)
                VALUES(:subject, :body, :date, :time, :posted_by_user_id) ';

            if (!empty($_FILES['image'])) {
                $uploadPath = uploadImage($subject);

                if (!empty($uploadPath)) {
                    $addEvent = 'INSERT INTO events(subject, body, image, date, time, posted_by_user_id)
                        VALUES(:subject, :body, :image, :date, :time, :posted_by_user_id) ';
                }
            }

            $stmt = $conn->prepare($addEvent);
            $stmt->bindParam(':subject', $subject);
            $stmt->bindParam(':body', $body);
            $stmt->bindParam(':date', $date);
            $stmt->bindParam(':time', $time);
            $stmt->bindParam(':posted_by_user_id', $userID);

            if (!empty($uploadPath)) {
                $stmt->bindParam(':image', $uploadPath);
            }

            $stmt->execute();
            $eventID = $conn->lastInsertId();

            foreach ($userTypes as $userType) {
                $query = 'INSERT INTO users_type_events (event_id, users_type_id)
                    VALUES (:event_id, :users_type_id)';
                $stmt = $conn->prepare($query);
                $stmt->bindParam(':event_id', $eventID);
                $stmt->bindParam(':users_type_id', $userType);
                $stmt->execute();
            }

            foreach ($colleges as $college) {
                $query = 'INSERT INTO colleges_events (event_id, college_id)
                    VALUES (:event_id, :college_id)';
                $stmt = $conn->prepare($query);
                $stmt->bindParam(':event_id', $eventID);
                $stmt->bindParam(':college_id', $college);
                $stmt->execute();
            }

            foreach ($organizations as $organization) {
                $query = 'INSERT INTO organizations_events (event_id, organization_id)
                    VALUES (:event_id, :organization_id)';
                $stmt = $conn->prepare($query);
                $stmt->bindParam(':event_id', $eventID);
                $stmt->bindParam(':organization_id', $organization);
                $stmt->execute();
            }

            setNotificationSession('EVENT_ADD', 'Event Added.');
            header('Location: ' . EVENT_LIST);
            exit;
        } else {
            header('Location: ' . EVENT_LIST);
            exit;
        }
    } else {
        header('Location: ' . EVENT_LIST);
        exit;
    }

