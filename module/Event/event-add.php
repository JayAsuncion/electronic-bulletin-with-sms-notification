<?php
    require_once('../../config/config.php');
    require_once('../../config/module-paths.php');
    require_once(APP_ROOT . '/module/Common/SessionService.php');
    require_once(APP_ROOT . '/module/Common/Database.php');
    require_once(APP_ROOT . '/module/Common/Logger.php');
    require_once(APP_ROOT . '/module/Common/Utilities.php');
?>

<?php
    if (!isLoggedIn()) {
        header('Location: ' . LOGIN_REGISTER_FORM);
        exit;
    }
?>

<?php
    require_once(APP_ROOT . '/module/Common/view/header.php')
?>

<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
    <?php
        require_once(APP_ROOT . '/module/Common/view/top-bar.php');
    ?>
    <div class="app-main">
        <?php
            $leftNavData = array('active_link' => 'Events');
            require_once(APP_ROOT . '/module/Common/view/left-nav.php');
        ?>

        <div class="app-main__outer">
            <div class="app-main__inner">
                <?php
                    $pageContentHeaderData = array(
                        'icon' => 'pe-7s-display2',
                        'title' => 'Add Event',
                        'description' => 'Add event to list of events',
                        'features' => 'Subject, body and image.',
                        'action_dropdown_options' => array(),
                        'back_button_href' => EVENT_LIST,
                        'options' => array(
                            'show_action_dropdown' => false,
                            'show_back_button' => true
                        )
                    );
                    require_once(APP_ROOT . '/module/Common/view/page-content-header.php')
                ?>
                <div class="row">
                    <div class="col-md-12 overflow-auto mb-5">
                        <form method="post" action="<?=EVENT_ADD_PROCESS?>" name="add_event" enctype="multipart/form-data">
                            <div class="position-relative form-group">
                                <label for="subject" class="font-weight-bold">Subject (<small class="font-weight-bold">Less than 255 characters</small>):</label>
                                <input class="form-control" type="text" id="subject" name="subject" value="" maxlength="255" required>
                            </div>
                            <div class="position-relative form-group">
                                <label for="body" class="font-weight-bold">Body:</label>
                                <textarea class="form-control" id="body" name="body" rows="6" required></textarea>
                            </div>
                            <div class="position-relative form-group">
                                <label for="image" class="font-weight-bold">Image(<small class="font-weight-bold">Less than 8MB</small>):</label>
                                <input type="file" id="image" name="image">
                            </div>
                            <div class="position-relative form-group">
                                <label class="font-weight-bold pr-3 d-block">User Types:</label>
                                <?php
                                $getUserTypes = 'SELECT * FROM users_type';
                                $stmt = $conn->prepare($getUserTypes);
                                $stmt->execute();

                                if ($stmt->rowCount() > 0) {
                                    $userTypes = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    echo '<div class="position-relative form-group">';

                                    foreach ($userTypes as $userType) {
                                        echo '
                                            <div class="custom-checkbox custom-control custom-control-inline">
                                                <input type="checkbox"
                                                    id="user_type_checkbox_' . $userType['user_type_id'] . '"
                                                    class="custom-control-input"
                                                    name="user_types[]"
                                                    value="' . $userType['user_type_id'] . '">
                                                <label class="custom-control-label"
                                                    for="user_type_checkbox_' . $userType['user_type_id'] . '">
                                                    ' . $userType['access_label'] . '
                                                </label>
                                            </div>
                                        ';
                                    }

                                    echo '</div>';
                                }
                                ?>
                            </div>
                            <div class="position-relative form-group">
                                <label class="font-weight-bold pr-3 d-block">Colleges:</label>
                                <?php
                                $getColleges = 'SELECT * FROM colleges';
                                $stmt = $conn->prepare($getColleges);
                                $stmt->execute();

                                if ($stmt->rowCount() > 0) {
                                    $colleges = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    echo '<div class="position-relative form-group">';

                                    foreach ($colleges as $college) {
                                        echo '
                                            <div class="custom-checkbox custom-control custom-control-inline">
                                                <input type="checkbox"
                                                    id="college_checkbox_' . $college['college_id'] . '"
                                                    class="custom-control-input"
                                                    name="colleges[]"
                                                    value="' . $college['college_id'] . '">
                                                <label class="custom-control-label"
                                                    for="college_checkbox_' . $college['college_id'] . '">
                                                    ' . $college['college_name'] . '
                                                </label>
                                            </div>
                                        ';
                                    }

                                    echo '</div>';
                                }
                                ?>
                            </div>
                            <div class="position-relative form-group">
                                <label class="font-weight-bold pr-3 d-block">Organizations:</label>
                                <?php
                                $getOrganizations = 'SELECT * FROM organizations';
                                $stmt = $conn->prepare($getOrganizations);
                                $stmt->execute();

                                if ($stmt->rowCount() > 0) {
                                    $organizations = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    echo '<div class="position-relative form-group">';

                                    foreach ($organizations as $organization) {
                                        echo '
                                            <div class="custom-checkbox custom-control custom-control-inline">
                                                <input type="checkbox"
                                                    id="organization_checkbox_' . $organization['organization_id'] . '"
                                                    class="custom-control-input"
                                                    name="organizations[]"
                                                    value="' . $organization['organization_id'] . '">
                                                <label class="custom-control-label"
                                                    for="organization_checkbox_' . $organization['organization_id'] . '">
                                                    ' . $organization['organization_name'] . '
                                                </label>
                                            </div>
                                            ';
                                    }

                                    echo '</div>';
                                }
                                ?>
                            </div>
                            <div class="position-relative form-group">
                                <button class="form-control btn btn-primary btn-md float-left">Add</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!--app-container-->

<?php
    require_once(APP_ROOT . '/module/Common/view/scripts.php')
?>

<?php
    require_once(APP_ROOT . '/module/Common/view/footer.php')
?>