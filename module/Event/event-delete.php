<?php
    require_once('../../config/config.php');
    require_once('../../config/module-paths.php');
    require_once(APP_ROOT . '/module/Common/SessionService.php');
    require_once(APP_ROOT . '/module/Common/Database.php');
    require_once(APP_ROOT . '/module/Common/Logger.php');
?>

<?php
    if (!isLoggedIn()) {
        header('Location: ' . LOGIN_REGISTER_FORM);
        exit;
    }
?>

<?php
    require_once(APP_ROOT . '/module/Common/view/header.php')
?>

<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
    <?php
        require_once(APP_ROOT . '/module/Common/view/top-bar.php');
    ?>
    <div class="app-main">
        <?php
            $leftNavData = array('active_link' => 'Events');
            require_once(APP_ROOT . '/module/Common/view/left-nav.php');
        ?>

        <div class="app-main__outer">
            <div class="app-main__inner">
                <?php
                    $id = $_GET['id'];
                    try {
                        $getEvent = 'SELECT events.*, CONCAT(first_name, " ", middle_name, " ", last_name) as "posted_by"
                            FROM events
                            INNER JOIN user_info ON events.posted_by_user_id = user_info.user_id
                            WHERE event_id = :event_id';
                        $stmt = $conn->prepare($getEvent);
                        $stmt->bindParam(':event_id', $id);
                        $stmt->execute();

                        if ($stmt->rowCount() == 1) {
                            $event = $stmt->fetch(PDO::FETCH_ASSOC);
                        }
                    } catch (PDOException $e) {
                        logError($e->getMessage());
                    }
                ?>

                <?php
                    $pageContentHeaderData = array(
                        'icon' => 'pe-7s-display2',
                        'title' => 'Delete Event ' . $id . ': ' . $event['subject'],
                        'description' => 'This will delete the event.',
                        'features' => 'Non-recoverable',
                        'action_dropdown_options' => array(),
                        'back_button_href' => EVENT_LIST,
                        'options' => array(
                            'show_action_dropdown' => false,
                            'show_back_button' => true
                        )
                    );
                    require_once(APP_ROOT . '/module/Common/view/page-content-header.php')
                ?>
                <div class="row">
                    <div class="col-md-12 overflow-auto mb-5">
                        <span class="d-block text-center text-danger font-size-lg font-weight-bold mb-4">
                            This will delete event "<?= $id; ?>: <?= $event['subject'] ?>" permanently.
                            Are you sure you want to continue?
                        </span>
                        <form method="post" action="<?=EVENT_DELETE_PROCESS?>" name="delete_event" enctype="multipart/form-data">
                            <input type="hidden" name="id" value="<?= $id ?>">
                            <div class="position-relative form-group text-center">
                                <button class="btn btn-danger btn-sm margin-h-center" style="width: 100px;">Delete</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!--app-container-->

<?php
    require_once(APP_ROOT . '/module/Common/view/scripts.php')
?>

<?php
    require_once(APP_ROOT . '/module/Common/view/footer.php')
?>