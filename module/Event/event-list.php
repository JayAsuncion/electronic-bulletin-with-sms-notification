<?php
    require_once('../../config/config.php');
    require_once('../../config/module-paths.php');
    require_once(APP_ROOT . '/module/Common/SessionService.php');
    require_once(APP_ROOT . '/module/Common/Database.php');
    require_once(APP_ROOT . '/module/Common/Logger.php');
    require_once(APP_ROOT . '/module/Common/Utilities.php');
?>

<?php
    if (!isLoggedIn()) {
        header('Location: ' . LOGIN_REGISTER_FORM);
        exit;
    }
?>

<?php
    require_once(APP_ROOT . '/module/Common/view/header.php')
?>

<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
    <?php
        require_once(APP_ROOT . '/module/Common/view/top-bar.php');
    ?>
    <div class="app-main">
        <?php
            $leftNavData = array('active_link' => 'Events');
            require_once(APP_ROOT . '/module/Common/view/left-nav.php');
        ?>

        <div class="app-main__outer">
            <div class="app-main__inner">
                <?php
                    $pageContentHeaderData = array(
                        'icon' => 'pe-7s-display2',
                        'title' => 'Events',
                        'description' => 'Manage event posts.',
                        'features' => 'Add/Edit/Delete Event Post',
                        'action_dropdown_options' => array(
                            array('href' => EVENT_ADD, 'label' => 'Add Event', 'icon' => 'fa fa-plus fa-xs')
                        ),
                        'back_button_href' => EVENT_LIST,
                        'options' => array(
                            'show_action_dropdown' => true,
                            'show_back_button' => false
                        )
                    );
                    require_once(APP_ROOT . '/module/Common/view/page-content-header.php')
                ?>
                <div class="row">
                    <div class="col-md-12 overflow-auto">
                        <table id="events_table" class="display table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th style="width: 20px;">ID</th>
                                    <th style="min-width: 80px;">Image</th>
                                    <th style="min-width: 80px;">Subject</th>
                                    <th>Body</th>
                                    <th style="min-width: 90px;">Date &amp; Time</th>
                                    <th style="min-width: 100px;">Posted By</th>
                                    <th style="min-width: 80px;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                try {
                                    $getEvents = 'SELECT events.*, CONCAT(first_name, " ", middle_name, " ", last_name) as "posted_by"
                                        FROM events
                                        INNER JOIN users_info ON events.posted_by_user_id = users_info.user_id';
                                    $stmt = $conn->prepare($getEvents);
                                    $stmt->execute();

                                    if ($stmt->rowCount() > 0) {
                                        $events = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                        foreach ($events as $event) {
                                            $event['body'] = strlen($event['body']) > 200 ?
                                                substr($event['body'], 0, 200) . "..."
                                                : $event['body'];

                                            echo '
                                            <tr>
                                                <td>' . $event['event_id'] . '</td>
                                                <td>';
                                            if (!empty($event['image'])) {
                                                echo '<img class="d-block img-fluid rounded" src="' . SITE_ROOT . $event['image'] . '" style="max-width: 100px;margin: auto;">';
                                            } else {
                                                echo '<span class="font-weight-bold d-block text-center text-danger">No Image To Preview.</span>';
                                            }
                                             echo '
                                                </td>
                                                <td>' . $event['subject'] . '</td>
                                                <td>' . $event['body'] . '</td>
                                                <td>' . $event['date'] . ' <br> @ ' . get12HourTime($event['time']) . '</td>
                                                <td>' . $event['posted_by'] . '</td>
                                                <td>
                                                <form class="d-inline-block mt-2 mb-2" action="'. EVENT_EDIT .'" method="get">
                                                    <button class="btn btn-success btn-sm">Edit</button>
                                                    <input type="hidden" name="id" value="' . $event['event_id'] . '">
                                                </form>
                                                <form class="d-inline-block mt-2 mb-2" action="'. EVENT_DELETE .'" method="get">
                                                    <button class="btn btn-danger btn-sm">Delete</button>
                                                    <input type="hidden" name="id" value="' . $event['event_id'] . '">
                                                </form>';
                                            if ($event['approved'] == '0') {
                                                echo '
                                                <form class="d-block mt-2 mb-2" action="'. EVENT_APPROVE_PROCESS .'" method="post">
                                                    <button class="btn btn-alternate btn-sm" style="width: 100%">Approve</button>
                                                    <input type="hidden" name="id" value="' . $event['event_id'] . '">
                                                </form>
                                                ';
                                            } else if ($event['approved'] == '1') {
                                                echo '
                                                <form class="d-block mt-2 mb-2" action="'. EVENT_DISAPPROVE_PROCESS .'" method="post">
                                                    <button class="btn btn-secondary btn-sm" style="width: 100%">Disapprove</button>
                                                    <input type="hidden" name="id" value="' . $event['event_id'] . '">
                                                </form>
                                                ';
                                            }
                                             echo '
                                                </td>
                                            </tr>
                                            ';
                                        }
                                    }
                                } catch (PDOException $e) {
                                    logError($e->getMessage());
                                }
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Subject</th>
                                    <th>Body</th>
                                    <th>Date &amp; Time</th>
                                    <th>Posted By</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!--app-container-->

<?php
    require_once(APP_ROOT . '/module/Common/view/scripts.php')
?>

<script>
    $(document).ready(function() {
        $('#events_table').DataTable( {
            "paging":   true,
            "ordering": true,
            "info":     false
        });
    });
</script>

<?php
    require_once(APP_ROOT . '/module/Common/view/footer.php')
?>