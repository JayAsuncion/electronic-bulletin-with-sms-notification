<?php
    require_once('../../config/config.php');
    require_once('../../config/module-paths.php');
    require_once(APP_ROOT . '/module/Common/SessionService.php');
    require_once(APP_ROOT . '/module/Common/Database.php');
    require_once(APP_ROOT . '/module/Common/Logger.php');
    require_once(APP_ROOT . '/module/Common/Utilities.php')
?>

<?php
    if (!isLoggedIn()) {
        header('Location: ' . LOGIN_REGISTER_FORM);
        exit;
    }

    $id = $_GET['id'];
    try {
        $getEvent = 'SELECT events.*, CONCAT(first_name, " ", middle_name, " ", last_name) as "posted_by"
                                    FROM events
                                    INNER JOIN users_info ON events.posted_by_user_id = users_info.user_id
                                    WHERE event_id = :event_id';
        $stmt = $conn->prepare($getEvent);
        $stmt->bindParam(':event_id', $id);
        $stmt->execute();

        if ($stmt->rowCount() == 1) {
            $event = $stmt->fetch(PDO::FETCH_ASSOC);
        }
    } catch (PDOException $e) {
        logError($e->getMessage());
    }

    $getUserTypesEvents = 'SELECT id, event_id, users_type_id 
        FROM users_type_events WHERE event_id = :event_id';
    $stmt = $conn->prepare($getUserTypesEvents);
    $stmt->bindParam(':event_id', $id);
    $stmt->execute();

    $userTypesEvents = array();

    if ($stmt->rowCount() > 0) {
        $userTypesEvents = $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    $getCollegesEvents = 'SELECT id, event_id, college_id 
        FROM colleges_events WHERE event_id = :event_id';
    $stmt = $conn->prepare($getCollegesEvents);
    $stmt->bindParam(':event_id', $id);
    $stmt->execute();

    $collegesEvents = array();

    if ($stmt->rowCount() > 0) {
        $collegesEvents = $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    $getOrganizationsEvents = 'SELECT id, event_id, organization_id 
        FROM organizations_events WHERE event_id = :event_id';
    $stmt = $conn->prepare($getOrganizationsEvents);
    $stmt->bindParam(':event_id', $id);
    $stmt->execute();

    $organizationsEvents = array();

    if ($stmt->rowCount() > 0) {
        $organizationsEvents = $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
?>

<?php
    require_once(APP_ROOT . '/module/Common/view/header.php')
?>

<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
    <?php
        require_once(APP_ROOT . '/module/Common/view/top-bar.php');
    ?>
    <div class="app-main">
        <?php
            $leftNavData = array('active_link' => 'Events');
            require_once(APP_ROOT . '/module/Common/view/left-nav.php');
        ?>

        <div class="app-main__outer">
            <div class="app-main__inner">
                <?php
                    $pageContentHeaderData = array(
                        'icon' => 'pe-7s-display2',
                        'title' => 'Edit Event ' . $id . ': ' . $event['subject'],
                        'description' => 'Edit event details.',
                        'features' => 'Edit event\'s subject, body and image.',
                        'action_dropdown_options' => array(),
                        'back_button_href' => EVENT_LIST,
                        'options' => array(
                            'show_action_dropdown' => false,
                            'show_back_button' => true
                        )
                    );
                    require_once(APP_ROOT . '/module/Common/view/page-content-header.php')
                ?>
                <div class="row">
                    <div class="col-md-12 overflow-auto mb-5">
                        <form method="post" action="<?=EVENT_EDIT_PROCESS?>" name="edit_event" enctype="multipart/form-data">
                            <div class="position-relative form-group">
                                <label for="subject" class="font-weight-bold">Subject (<small class="font-weight-bold">Less than 255 characters</small>):</label>
                                <input class="form-control" type="text" id="subject" name="subject" value="<?= $event['subject']; ?>" maxlength="255" required>
                            </div>
                            <div class="position-relative form-group">
                                <label for="body" class="font-weight-bold">Body:</label>
                                <textarea class="form-control" id="body" name="body" rows="6" required><?= $event['body']; ?></textarea>
                            </div>
                            <div class="position-relative form-group">
                                <label for="date" class="font-weight-bold">Date & Time Posted: </label>
                                <span><?= $event['date']; ?></span> @ <span><?= get12HourTime($event['time']); ?></span>
                            </div>
                            <div class="position-relative form-group">
                                <label for="image" class="font-weight-bold">Image(<small class="font-weight-bold">Less than 8MB</small>):</label>
                                <input type="file" id="image" name="image">
                                <?php
                                if (!empty($event['image'])) {
                                    echo '<img class="d-block img-fluid border border-secondary rounded" src="' . SITE_ROOT . $event['image'] . '" style="max-width: 200px;margin: auto;">';
                                    echo '<span class="d-block text-center">' . $event['image'] . '</span>';
                                } else {
                                    echo '<span class="font-weight-bold d-block text-center text-danger m-4">No Image To Preview.</span>';
                                }
                                ?>
                            </div>
                            <div class="position-relative form-group">
                                <label class="font-weight-bold pr-3 d-block">User Types:</label>
                                <?php
                                $getUserTypes = 'SELECT * FROM users_type';
                                $stmt = $conn->prepare($getUserTypes);
                                $stmt->execute();

                                if ($stmt->rowCount() > 0) {
                                    $userTypes = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    echo '<div class="position-relative form-group">';

                                    foreach ($userTypes as $userType) {
                                        $isCheckedFlag = '';
                                        foreach ($userTypesEvents as $userTypesEvent) {
                                            if ($userType['user_type_id'] == $userTypesEvent['users_type_id']) {
                                                $isCheckedFlag = 'checked';
                                            }
                                        }

                                        echo '
                                                <div class="custom-checkbox custom-control custom-control-inline">
                                                    <input type="checkbox"
                                                        id="user_type_checkbox_' . $userType['user_type_id'] . '"
                                                        class="custom-control-input"
                                                        name="user_types[]"
                                                        value="' . $userType['user_type_id'] . '"
                                                        ' . $isCheckedFlag . '
                                                        >
                                                    <label class="custom-control-label"
                                                        for="user_type_checkbox_' . $userType['user_type_id'] . '">
                                                        ' . $userType['access_label'] . '
                                                    </label>
                                                </div>
                                            ';
                                    }

                                    echo '</div>';
                                }
                                ?>
                            </div>
                            <div class="position-relative form-group">
                                <label class="font-weight-bold pr-3 d-block">Colleges:</label>
                                <?php
                                $getColleges = 'SELECT * FROM colleges';
                                $stmt = $conn->prepare($getColleges);
                                $stmt->execute();

                                if ($stmt->rowCount() > 0) {
                                    $colleges = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    echo '<div class="position-relative form-group">';

                                    foreach ($colleges as $college) {
                                        $isCheckedFlag = '';
                                        foreach ($collegesEvents as $collegesEvent) {
                                            if ($college['college_id'] == $collegesEvent['college_id']) {
                                                $isCheckedFlag = 'checked';
                                            }
                                        }

                                        echo '
                                            <div class="custom-checkbox custom-control custom-control-inline">
                                                <input type="checkbox"
                                                    id="college_checkbox_' . $college['college_id'] . '"
                                                    class="custom-control-input"
                                                    name="colleges[]"
                                                    value="' . $college['college_id'] . '"
                                                    ' . $isCheckedFlag . '>
                                                <label class="custom-control-label"
                                                    for="college_checkbox_' . $college['college_id'] . '">
                                                    ' . $college['college_name'] . '
                                                </label>
                                            </div>
                                        ';
                                    }

                                    echo '</div>';
                                }
                                ?>
                            </div>
                            <div class="position-relative form-group">
                                <label class="font-weight-bold pr-3 d-block">Organizations:</label>
                                <?php
                                $getOrganizations = 'SELECT * FROM organizations';
                                $stmt = $conn->prepare($getOrganizations);
                                $stmt->execute();

                                if ($stmt->rowCount() > 0) {
                                    $organizations = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                    echo '<div class="position-relative form-group">';

                                    foreach ($organizations as $organization) {
                                        $isCheckedFlag = '';
                                        foreach ($organizationsEvents as $organizationsEvent) {
                                            if ($organization['organization_id'] == $organizationsEvent['organization_id']) {
                                                $isCheckedFlag = 'checked';
                                            }
                                        }

                                        echo '
                                            <div class="custom-checkbox custom-control custom-control-inline">
                                                <input type="checkbox"
                                                    id="organization_checkbox_' . $organization['organization_id'] . '"
                                                    class="custom-control-input"
                                                    name="organizations[]"
                                                    value="' . $organization['organization_id'] . '"
                                                    ' . $isCheckedFlag . '>
                                                <label class="custom-control-label"
                                                    for="organization_checkbox_' . $organization['organization_id'] . '">
                                                    ' . $organization['organization_name'] . '
                                                </label>
                                            </div>
                                            ';
                                    }

                                    echo '</div>';
                                }
                                ?>
                            </div>
                            <input type="hidden" name="id" value="<?= $id ?>">
                            <div class="position-relative form-group">
                                <button class="form-control btn btn-primary btn-md float-left">Edit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!--app-container-->

<?php
    require_once(APP_ROOT . '/module/Common/view/scripts.php')
?>

<?php
    require_once(APP_ROOT . '/module/Common/view/footer.php')
?>