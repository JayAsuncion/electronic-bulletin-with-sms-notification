<?php
    require_once('../../config/config.php');
    require_once('../../config/module-paths.php');
    require_once(APP_ROOT . '/module/Common/SessionService.php');
    require_once(APP_ROOT . '/module/Common/Database.php');
    require_once(APP_ROOT . '/module/Common/Logger.php');

    if (!isLoggedIn()) {
        header('Location: ' . LOGIN_REGISTER_FORM);
        exit;
    }

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $id = isset($_POST['id']) ? $_POST['id'] : 0;

        if (!empty($id)) {
            $approveAnnouncement = 'UPDATE events SET approved = 1 WHERE event_id = :event_id';
            $stmt = $conn->prepare($approveAnnouncement);
            $stmt->bindParam(':event_id', $id);
            $stmt->execute();

            setNotificationSession('EVENT_APPROVE', 'Event Approved.');
            header('Location: ' . EVENT_LIST);
            exit;
        } else {
            header('Location: ' . ANNOUNCEMEEVENT_LISTNT_LIST);
            exit;
        }
    } else {
        echo "2";
        header('Location: ' . EVENT_LIST);
        exit;
    }