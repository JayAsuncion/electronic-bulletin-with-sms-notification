<?php
    require_once('../../config/config.php');
    require_once('../../config/module-paths.php');
    require_once(APP_ROOT . '/module/Common/SessionService.php');
    require_once(APP_ROOT . '/module/Common/Database.php');
    require_once(APP_ROOT . '/module/Common/Logger.php');
    require_once(APP_ROOT . '/module/Common/FileUploader.php');

    if (!isLoggedIn()) {
        header('Location: ' . LOGIN_REGISTER_FORM);
        exit;
    }

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $id = isset($_POST['id']) ? $_POST['id'] : 0;
        $subject = isset($_POST['subject']) ? $_POST['subject'] : 0;
        $body = isset($_POST['body']) ? $_POST['body'] : 0;

        $userTypes = isset($_POST['user_types']) ? $_POST['user_types'] : 0;
        $colleges = isset($_POST['colleges']) ? $_POST['colleges'] : 0;
        $organizations = isset($_POST['organizations']) ? $_POST['organizations'] : 0;

        if (!empty($id) && !empty($subject) && !empty($body)) {
            $editEvent = 'UPDATE events SET subject = :subject, body = :body WHERE event_id = :event_id';

            if (!empty($_FILES['image'])) {
                $uploadPath = uploadImage($subject);

                if (!empty($uploadPath)) {
                    $editEvent = 'UPDATE events SET subject = :subject, body = :body, image = :image WHERE event_id = :event_id';
                }
            }

            $stmt = $conn->prepare($editEvent);
            $stmt->bindParam(':subject', $subject);
            $stmt->bindParam(':body', $body);
            $stmt->bindParam(':event_id', $id);

            if (!empty($uploadPath)) {
                $stmt->bindParam(':image', $uploadPath);
            }

            $stmt->execute();

            $deleteQuery = 'DELETE FROM users_type_events
                WHERE event_id = :event_id';
            $stmt = $conn->prepare($deleteQuery);
            $stmt->bindParam(':event_id', $id);
            $stmt->execute();

            $deleteQuery = 'DELETE FROM colleges_events
                WHERE event_id = :event_id';
            $stmt = $conn->prepare($deleteQuery);
            $stmt->bindParam(':event_id', $id);
            $stmt->execute();

            $deleteQuery = 'DELETE FROM organizations_events
                WHERE event_id = :event_id';
            $stmt = $conn->prepare($deleteQuery);
            $stmt->bindParam(':event_id', $id);
            $stmt->execute();

            foreach ($userTypes as $userType) {
                $query = 'INSERT INTO users_type_events (event_id, users_type_id)
                    VALUES (:event_id, :users_type_id)';
                $stmt = $conn->prepare($query);
                $stmt->bindParam(':event_id', $id);
                $stmt->bindParam(':users_type_id', $userType);
                $stmt->execute();
            }

            foreach ($colleges as $college) {
                $query = 'INSERT INTO colleges_events (event_id, college_id)
                    VALUES (:event_id, :college_id)';
                $stmt = $conn->prepare($query);
                $stmt->bindParam(':event_id', $id);
                $stmt->bindParam(':college_id', $college);
                $stmt->execute();
            }

            foreach ($organizations as $organization) {
                $query = 'INSERT INTO organizations_events (event_id, organization_id)
                    VALUES (:event_id, :organization_id)';
                $stmt = $conn->prepare($query);
                $stmt->bindParam(':event_id', $id);
                $stmt->bindParam(':organization_id', $organization);
                $stmt->execute();
            }


            setNotificationSession('EVENT_EDIT', 'Event Edited.');
            header('Location: ' . EVENT_EDIT . '?id=' . $id);
            exit;
        }
    } else {
        header('Location: ' . EVENT_LIST);
        exit;
    }