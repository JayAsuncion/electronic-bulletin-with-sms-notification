<?php
define('BLANK_TEMPLATE', SITE_ROOT . '/module/Common/view/blank-template.php');
define('LOGIN_REGISTER_FORM', SITE_ROOT);

define('EVENT_LIST', SITE_ROOT . '/module/Event/event-list.php');
define('EVENT_ADD', SITE_ROOT . '/module/Event/event-add.php');
define('EVENT_ADD_PROCESS', SITE_ROOT . '/module/Event/event-add-process.php');
define('EVENT_EDIT', SITE_ROOT . '/module/Event/event-edit.php');
define('EVENT_EDIT_PROCESS', SITE_ROOT . '/module/Event/event-edit-process.php');
define('EVENT_DELETE', SITE_ROOT . '/module/Event/event-delete.php');
define('EVENT_DELETE_PROCESS', SITE_ROOT . '/module/Event/event-delete-process.php');
define('EVENT_APPROVE_PROCESS', SITE_ROOT . '/module/Event/event-approve-process.php');
define('EVENT_DISAPPROVE_PROCESS', SITE_ROOT . '/module/Event/event-disapprove-process.php');

define('ANNOUNCEMENT_LIST', SITE_ROOT . '/module/Announcement/announcement-list.php');
define('ANNOUNCEMENT_ADD', SITE_ROOT . '/module/Announcement/announcement-add.php');
define('ANNOUNCEMENT_ADD_PROCESS', SITE_ROOT . '/module/Announcement/announcement-add-process.php');
define('ANNOUNCEMENT_EDIT', SITE_ROOT . '/module/Announcement/announcement-edit.php');
define('ANNOUNCEMENT_EDIT_PROCESS', SITE_ROOT . '/module/Announcement/announcement-edit-process.php');
define('ANNOUNCEMENT_DELETE', SITE_ROOT . '/module/Announcement/announcement-delete.php');
define('ANNOUNCEMENT_DELETE_PROCESS', SITE_ROOT . '/module/Announcement/announcement-delete-process.php');
define('ANNOUNCEMENT_APPROVE_PROCESS', SITE_ROOT . '/module/Announcement/announcement-approve-process.php');
define('ANNOUNCEMENT_DISAPPROVE_PROCESS', SITE_ROOT . '/module/Announcement/announcement-disapprove-process.php');

define('USER_LIST', SITE_ROOT . '/module/User/user-list.php');
define('USER_ADD', SITE_ROOT . '/module/User/user-add.php');
define('USER_ADD_PROCESS', SITE_ROOT . '/module/User/user-add-process.php');
define('USER_EDIT', SITE_ROOT . '/module/User/user-edit.php');
define('USER_EDIT_PROCESS', SITE_ROOT . '/module/User/user-edit-process.php');
define('USER_DELETE', SITE_ROOT . '/module/User/user-delete.php');
define('USER_DELETE_PROCESS', SITE_ROOT . '/module/User/user-delete-process.php');

define('COLLEGE_LIST', SITE_ROOT . '/module/College/college-list.php');
define('COLLEGE_ADD', SITE_ROOT . '/module/College/college-add.php');
define('COLLEGE_ADD_PROCESS', SITE_ROOT . '/module/College/college-add-process.php');
define('COLLEGE_EDIT', SITE_ROOT . '/module/College/college-edit.php');
define('COLLEGE_EDIT_PROCESS', SITE_ROOT . '/module/College/college-edit-process.php');

define('ORGANIZATION_LIST', SITE_ROOT . '/module/Organization/organization-list.php');
define('ORGANIZATION_ADD', SITE_ROOT . '/module/Organization/organization-add.php');
define('ORGANIZATION_ADD_PROCESS', SITE_ROOT . '/module/Organization/organization-add-process.php');
define('ORGANIZATION_EDIT', SITE_ROOT . '/module/Organization/organization-edit.php');
define('ORGANIZATION_EDIT_PROCESS', SITE_ROOT . '/module/Organization/organization-edit-process.php');
define('ORGANIZATION_MEMBER_LIST', SITE_ROOT . '/module/Organization/organization-member-list.php');
define('ORGANIZATION_MEMBER_ADD', SITE_ROOT . '/module/Organization/organization-member-add.php');
define('ORGANIZATION_MEMBER_ADD_PROCESS', SITE_ROOT . '/module/Organization/organization-member-add-process.php');
define('ORGANIZATION_MEMBER_DELETE', SITE_ROOT . '/module/Organization/organization-member-delete.php');
define('ORGANIZATION_MEMBER_DELETE_PROCESS', SITE_ROOT . '/module/Organization/organization-member-delete-process.php');