<?php
// Database
define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', 'root');
define('DB_NAME', 'e-bulletin');

define('APP_ROOT', dirname(dirname(__FILE__)));
define('SITE_ROOT', 'http://localhost/e-bulletin');
define('SITE_NAME', 'E-Bulletin');

// Time Zone
date_default_timezone_set('Asia/Manila');