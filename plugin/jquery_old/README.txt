Compressed and uncompressed copies of jQuery files are available. 

> The uncompressed file is best used during development or debugging; 
> the compressed file saves bandwidth and improves performance in production. 

You can also download a sourcemap file for use when debugging with a compressed file. 