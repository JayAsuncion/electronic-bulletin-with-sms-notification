JS files 		Popper 		jQuery
bootstrap.bundle.js
bootstrap.bundle.min.js
			Included 	Not included
bootstrap.js
bootstrap.min.js
			Not included 	Not included

Show components requiring JavaScript
    > Alerts for dismissing
    > Buttons for toggling states and checkbox/radio functionality
    > Carousel for all slide behaviors, controls, and indicators
    > Collapse for toggling visibility of content
    > Dropdowns for displaying and positioning (also requires Popper.js)
    > Modals for displaying, positioning, and scroll behavior
    > Navbar for extending our Collapse plugin to implement responsive behavior
    > Tooltips and popovers for displaying and positioning (also requires Popper.js)
    > Scrollspy for scroll behavior and navigation updates
